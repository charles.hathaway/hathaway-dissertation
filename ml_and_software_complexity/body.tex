\section{Introduction}
When considering bottom-up approaches to solving computer science problems, it is hard to ignore the massive surge in the popularity and success of machine learning.
Many of these techniques were inspired by examining the living world, and using the processes observed in organisms and biological networks to create computational analogs.

Consider the case of genetic algorithms; this is a type of algorithm in which the title itself betrays its similarity to the process of Darwin's theory of evolution.
As the algorithm runs, it optimizes the "genetic makeup" of solution domain using a fitness function.
Those genetic configuration which are the fittest survive, and the less optimal solutions get weeded out.

One of the main challenges in this bottom up approach of solving problems is creating the representation of the solution domain.
For example, using a genetic algorithm to identify section of code which were likely to possess bugs would require us to transform the source code into a meaningful representation of fitness.
Simply using the binary representation of a source file would be meaningless; keywords would be ignored, the structure of application would be lost, changing of variable names would have an arbitrary impact on the fitness of the source file.

This chapter examines several machine learning techniques to determine if this new bio-inspired body of work can be used to enhance the somewhat pigeon-holed field of code analysis.

\section{Contrast with Previous Works}

A large majority of code analysis is done using static code analysis, semantic analysis, and other top-down approaches which require the program doing the analysis to have a complete understanding of the material it is analyzing.
The idea of using software complexity for such a task underwent massive work in the early 90's, but for the past 20 or so years has been dormant due to the not-so-great results.
Using complexity metrics as a way of representing a solution domain allows the use of bottom-up, machine learning, techniques which have been the focus of so many researchers work.

Many code analysis tools require a complete understanding of the source code; that is to say they require the definitions of every method, the type of variable (or the ability to infer the type), and sometimes even the source code of every method to set up boundary conditions.
Most software complexity metrics do not require this complete understanding; McCabe's only examines the method in question, IC-WMC\footnote{Interactive Complexity is discussed in chapter \ref{interactive_complexity}} only requires the meta-data around a project and doesn't need any source code, and the lines of code just needs to count the number of newlines in the source file.

Many of these metrics are relatively cheap to calculate, which means faster computation and less overhead.
Additionally, many of these metrics don't require an understanding of the language of the source code; they are language agnostic.
This makes them particularly useful for languages that have a small userbase (such as domain-specific languages), are new to the world and don't have much support, or have poorly defined grammars which makes parsers hard to create.

\section{Case Study}

It would be possible to do this analysis at various levels of granularity.
These levels would be:

% A diagram would be helpful here
\begin{enumerate}
\item Project
\item Release
\item Commit
\item File/class
\item Function
\item Line
\end{enumerate}

Each of these granularities has pros and cons.
The more granular, the more data points we get, and the more accurate our results will be.
However, this also represents a significant increase in computation time.
In addition, many of the features we use only work for a particular granularity.
For example, all the interactive complexity metrics work at an entire project scale; you can redo the calculation at each commit and release, but you can't have a different value for each function.
Likewise, Cyclomatic complexity is specific to a method; to find a complexity for a class, you would need to have a mechanism to combine the complexity of the individual method complexities.

To address this, we propose the following techniques for scaling a measure up (from method to class) and scaling a measure down (from project to method).
By doing this, we can run the analysis at which level is deemed most appropriate.

Scaling up can be done by taking the median of all values below the level we are looking at.
For example, to get the cyclomatic complexity of a project, we take the median complexity of all files; to find the complexity of a file, we take the median complexity of all classes within the file.
To fine the complexity of a class, we take the median complexity of all methods within that class.

Scaling down can be done by simply labeling all subparts of the item in question with the same value as the parent.
For example, if the IC-DIT of a project is 6, we can say class has a IC-DIT of 6, and each line within that class has a IC-DIT of 6.

Using this technique, we calculate features for files during this case study.
This will allow us to cluster classes which are highly likely to have a fault, a granularity which would be helpful for developers, without making too many generalizations about higher-level granularities (if every line has the same 7/10 features, why bother at that level?).

\subsection{Features}

Each of these features are explained in more detailed elsewhere.
For details of interactive complexity metrics (IC-), see the Interactive Complexity chapter.
For the other metrics, see the Literature Review chapter.
A brief summary is provided alongside each metric, but should only be used a refresher.

\begin{enumerate}
\item Cyclomatic Complexity -- number of edges in the control flow graph
\item Data Flow Complexity -- number of variable definitions in different branches of the control flow graph
\item Lines of Code -- size of the file, in lines
\item CK-NOC -- number of children a class is inherited by
\item CK-DIT -- depth of the inheritance tree for a particular class
\item CK-WMC -- number of methods within a class
\item CK-RFC -- number of methods which can be called for a public method call
\item CK-CBO -- median of the cohesion between this class and all other classes in the project
\item CK-LCOM1 -- median of the methods which are not used in each other class
\item IC-NOC -- project wide, the number of projects which depend on this one
\item IC-DIT -- project wide, the distance to the first project depended on by this project which has no dependencies
\item IC-WMC -- project wide, the count of projects that we depend on
\item IC-RFC -- project wide, the number of publicly exposed functions (this can be refined to be file specific)
\item IC-CBO -- project wide, the number of cycles this project is part of within dependencies of this project
\end{enumerate}

\subsection{Methodology}

The Software Metrics Framework (SMF) \cite{software_metrics_framework} was used to gather the complexity metrics for each project.
After the metics were calculated, scikit-learn \cite{scikit-learn} was used to perform the various machine learning algorithms. 
The scripts that were written to facilitate the case study are available in the SMF repository.

For each algorithm, the available training sets are permuted such that every training set is used as a testing set at least once.
The median accuracy of all results is then reported for each technique in the conclusion.
Three machine learning techniques, described below, were used.

The goal is to identify source files as having a high, medium, or low rate of faults.
Labels were assigned such that source files with less than (mean -- one standard deviation) were given a LOW fault rate, source files with faults between (mean -- one standard deviation $<$ value $<$ mean + one standard deviation) were assigned a label of MED, and source files with total faults greater than (mean + one standard deviation) had a HIGH fault rate.
The scope of this case study was limited to ~1000 commits (checkpoints in which a developer publishes their work to the world).
The first of these commits had 626 HIGH, 1685 MED, and 3403 LOW fault rate files.
This helps us establish a baseline; assigning LOW to all files would give us an accuracy of 59.6\%.

\subsubsection{K-Nearest Neighbors}

K-Nearest Neighbors (KNN) is a machine learning approach which attempts to label data points based on their location to each other in a multi-dimensional space.
In short, given a data point with a list of features (in this case, GOFAI software complexity metrics) we find the nearest related neighbors to which we know the data for, and label the unknown datapoint based on the labels of the neighbors.

There are a few caveats to be aware, some of which were stumbled upon during the use of this technique in the analysis.
First and foremost, the data must be normalized.
Without doing these, some features (such as IC-RFC, which has values in the thousands) would be vastly over influential in finding the nearest neighbor.
Thankfully, scikit-learn provided tools to preprocess and normalize the data.

Another consideration is repetition of data.
During the analysis, initial results put the labeling accuracy at 70-80\%; this is just over the naive baseline of 60\%.
The cause of this of poor classification was the repetition of data points as a result of including data for every source file within a single training file, rather than just including the data for each source file which had had it's values change.
By changing this, the measured accuracy drastically improved.

\subsubsection{Multilayer Perceptron Neural Network}

Perhaps one of the most obviously biology inspired machine learning algorithms around, neural networks took their inspiration from the brain.
Each input is mapped to one or more input neuron, and each label to an output neuron.
In between, there are several layers of hidden neurons.
The connections between these neurons have activation weights, which are used to influence when the neuron fires.
By manipulating these weights through a feedback mechanism, the network can be trained on some data, then tested on a live data set.

Initial tested on a subset of our data, abysmal results nearing the baseline were obtained.
However, when ran on the complete data set, which had many more data points, the result got substantially better.
Initial results were at an accuracy of around 60\%, with a sample size of 10 training files (6k data points per file).
With the complete data set, 1000 training files, the results improved to near perfect accuracy ($>$ .995).

\subsubsection{Stochastic Gradient}

Lastly, a stochastic gradient classifier was tested.
This type of algorithm is usually better suited for corpuses much larger than the one in test, and such performed fairly badly.
It is possible that this could be improved by tweaking hyper-parameters.

The average performance for this technique was 52\%, however it should be noted that a majority of the results were at the 62\% mark with several training configurations resulting in accuracy of below 20\%.

\section{Conclusion}

For most of the tested techniques, we can show a substantial improvement over the baseline for predicting fault rate of source files.
KNearestNeighbors had the worst accuracy, measured at around 75\%, which was drastically improved by reducing noise.
After removing duplicate entries, this algorithm performed much better.

Neural networks gave consistently good results in both the noisy and reduced noise sets.
However, it required a much larger dataset and took much longer to run (hours, versus minutes for the other algorithms).

\begin{table}
	\captionof{table}{Results of different machine learning algorithms on the data set}
	\begin{longtable}{| c |l | l |}
		\hline
		Algorithm & Accuracy & Window-size \\
			\hline
		KNearestNeighbors (all noise) & 0.75 & 1 \\
			\hline
		KNearestNeighbors (reduced noise) & 0.9996 & 1 \\
			\hline
		Multilayer Perceptron (all noise) & 0.9996 & 1 \\
			\hline
		Multilayer Perceptron (reduced noise) & 0.9996 & 1 \\
			\hline
		Stoachastic Gradient Descent (all noise) & .52 & 1 \\
			\hline
		Stoachastic Gradient Descent (reduced noise) & .52 & 1 \\
		\hline
	\end{longtable}
\end{table}

While these results are good, the value of them is marginal.
Knowing what file will cause a fault within a software product is not fine grained enough to be of extreme use to most developers.
Future work will have to examine the accuracy of these methods at a high granularity.
