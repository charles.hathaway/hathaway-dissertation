\section{Introduction}

Software complexity metrics are specialized for code; to begin forming connections between this domain and the more general domain of complexity, and thus make it easier to think about in term of biological analogs, a thorough understanding of its roots are needed.
This chapter is dedicated to exploring the background of software complexity metrics; from the first metrics proposed by McCabe, up through metrics being published in 2016.

In addition to summarizing many of the important pieces of literature over the last 40 years, this chapter also groups those metrics into three categories.
These three categories represent the conventional top-down approach to discussing the metrics; talking about function level control-flow complexity, function level data-flow complexity, and function-flow complexity metrics.
Later in this dissertation, a new class of metrics (ecosystem-level) are discussed, but we will need a thorough review of the existing literature before moving to that definition.


\section{History of Software Complexity Metrics}

In this first section, we will examine the development of software complexity metrics as they gradually emerged through the years.
By doing so, we can see how the determination of which features or structures should be counted has become the focus of these metrics.  
We conclude this section by summarizing a variety of metrics that have been published, and discuss the relationship between the metrics and the historical moment in which they were invented.

\subsection{The 70's}
% Discussion of why the metric exists

% T. J. McCabe, "A complexity measure," IEEE TRANS-
% ACTIONS ON SOFTWARE ENGINEERING, vol. SE-2,
% no. 4, pp. 308–320, 1976.
McCabe proposed one of the first software complexity metrics. It is still considered one of the most important, typically cited in any literature related to software complexity \cite{mccabe1976complexity}.
This metric is simply defined as $C=e-n+2p$, where e is edges, n is nodes, and p is components within a control-flow graph of a particular program.
Although providing an interesting approach to complexity analysis, there have been numerous criticisms of this metric, including the ones done by Weyuker \cite{weyuker1988evaluating} and Shepperd \cite{shepperd1988critique}.

% M. H. Halstead, "Elements of software science," 1977.
Following McCabes metric, Halstead defined his own metric which gives less influence to the flow of a program, and more to the number of "moving parts" within the program \cite{halstead1977elements}.
This metric counts the number of unique and total operators and operands to determine a complexity measure.
Halstead's aim was to focus more on the mental difficulty of tracking a large number of variables through a program.
Although his idea may be correct, the metric itself never became as popular as McCabe's. Perhaps it's simply not as interesting as control flow, and on the surface doesn't seem like it would be as accurate.

Woodward et al \cite{woodward1979measure} proposed a measure similar to McCabes, called the Knot measure, but instead of constructing a directed control flow graph they counted the number of times program execution would overlap if you were to draw arrows following the execution of the program along the margins of a printed copy of the paper.
They claimed a number of advantages to McCabes metric, even addressing some of the issues that would be brought to light by Weyuker 9 years later.
Their primary claim was that by utilizing the physical layout of the document text, the ordering of the nodes within the code would matter; that is, a reordering of the code could result in a more or less complex piece of code.
It should be noted that this metric was designed primarily for programming systems that made heavy use of goto statements; lacking goto statements, all programs would have a Knot measure of 0.


% There is some discussion in this era about why we need measures; often just written as:
%  "An increasing need to..."
%S. J. Amster, E. J. Davis, B. N. Dickman, and J. P. Kuoni, "An ex- periment in automatic quality evaluation of software," in Proc. Symp. Computer Software Eng., MRI Symposia Series, vol. XXIV, J. Fox, Ed., Polytechnic Institute of New York, Apr. 1976, pp. 171-197.


% Not a metric, but discussion of psycological impact taking into account McCabe and Halstead
A very interesting comparison of the two most important metrics from this time period (Halstead's and McCabe's) was done in 1979 by Curtis \cite{curtis1979measuring}, and found some illuminating correlations.
The correlations between accuracy/speed and these metrics suggest that there is some truth to these metrics in most cases, however, they also indicated that there were many cases where the metrics lost accuracy, namely when attempting to evaluate code written by experienced programmers.
This suggests that multiple factors influence a programmers ability to understand code, including existing knowledge of the codebase.
Even with the ideal cases, the correlation between the metrics and the results of the code generated by the subjects was still weak.

\subsection{The 80's}

% E. J. Weyuker, "Evaluating software complexity mea-
% sures," IEEE TRANSACTIONS ON SOFTWARE ENGI-
% NEERING,, vol. 14, no. 9, pp. 1357–1365, 1988.
One of the most pivotal works in this field was done in 1988 by Weyuker; she setup a framework for evaluating complexity metrics, and identified a number of flaws in the most pivotal existing metrics \cite{weyuker1988evaluating}.
In addition to demonstrating what she believed to be flaws in the McCabes metric, Weyuker also analyzed Data Flow complexity, statement count, and effort measure (which is just another phrase for Halstead's measure).
Given the time period in which this article came out, one may believe that there were no significant contributions made to the field between 1979 and 1988; although I found several interesting articles published in 1988, I failed to find anything that Weyuker would have had access to in this given time frame.

% Good source for other sources
% Reference to longer design phases being better...
%  Contrast with Agile
Kearney et al \cite{kearney1986software} made sharp criticism of software complexity as a field in their 1986 article.
Among other things, they point out that most of the proposed metrics have no particular use in mind; rather, they are just trying to solve a very ill-defined problem.
Furthermore, they bring issues forth regarding many previous experiments done in the field of software complexity.
Before any more research is conducted, this paper should be addressed.

% Critique of McCabes
Whereas Kearney discussed the software complexity field as whole, Shepperd brought up concerns regarding a specific metric \cite{shepperd1988critique}.
He goes so far as to assert that, even though it is universally used as "bad metric", lines of code may be a better indicator than cyclomatic complexity.
However, Shepperd agrees with Kearney in the fact that it is much more important for metrics to have a solid validation if they are to be seen as serious.

% Can we use some sort of ML technique to help identify useful attributes?

In attempt to clarify goals and identify objectives complexity metrics can achieve, several metrics attempt to represent how difficult it would be to test all execution paths within a program.
One such metric is NPath, created by Sibley et al \cite{nejmeh1988npath}.
This metric is similar to McCabes, based on the control flow graph that is used to calculate McCabes metric, but gives more weight to type of control flow statement and their sequence.
This paper isn't particularly pivotal, but represents the thoughts of researchers at this time.

\subsection{The 90's}

% This doesn't make sense; check citation
% E. I. Oviedo, "Control flow, data flow and program com-
% plexity," in Software engineering metrics I, McGraw-
% Hill, Inc., 1993, pp. 52–65.
%\cite{wand1990ontological}

% OOP metrics; when did OOP come into existence?
Up until this time, most previous metrics concerned themselves with functional programming.
That is, the metrics looked at the control flow of the program, or the number of operands, or other items which are not as applicable in an object-oriented systems.
Chidamber and Kemerer proposed a series of OO system metrics in their 1994 paper \cite{chidamber1994metrics}.
However, much more interesting than their metrics themselves is the background given in this paper.
In addition to citing the above mentioned critiques, they identify a number of papers which answer several of the questions put forth.
Namely, they speak of using the theoretical base proposed by Bunge \cite{bunge1977treatise} \cite{bunge1979world}.

% Contains many references to other metrics; READ!
% Very long paper... meh.
% References several works that "verify functional metrics"; do these
%  hold up to the blasting from Kearney?
%\cite{li1993object}

One of the issues with many previous metrics is the lack of clear definitions for terms such as complexity.
Briand et al attempt to create a vocabulary for talking about software complexity (and other things) in their 1996 paper \cite{briand1996property}.
Their work is mathematically rigorous, and focuses very much on defining these terms in such a way as to avoid any confusion.
Whether or not these definitions get adopted is yet to be determined.

Briant et al proposed a framework to evaluate existing object-oriented measurements in 1999 \cite{briand1999unified}.
They attempt to solidify several terms used in existing measurement frameworks, especially the phrase "coupling".
Briant proposes 6 criteria to help classify frameworks:
\begin{enumerate}
	\item Type of connection
	\item Locus of impact
	\item Granularity of measure
	\item Stability of server (class, interface)
	\item Direct or indirect coupling
	\item Inheritance
\end{enumerate}

% Good summary?
% Same guy who wrote the book
Fenton has done much work in recent years to help solidify the field of software complexity.
In his 1999 paper he proposed an interesting approach to predicting fault rate (along with making a distinction between pre and post release faults) \cite{fenton1999software}.
After observing that industry had not taken to more sophisticated measures (such as McCabes or Halstead's), Fenton tried utilizing a Bayesian Belief Network to simulate the unknowns in software development.
This paper in particular claims great success in answering a number of hypotheses, and producing what the author refers to as "surprising" results.

% Not directly related; personal interest
%\cite{wolfgang1994design}

\subsection{The 00's}

Most of the effort in the 2000's was spent on verifying existing measures, rather than creating new ones.


Following his 1999 publication, Fenton published an article in 2000 that reiterated the need to refocus the software engineering metric community on using existing measures alongside new technologies to provide improved results \cite{fenton2000software}.

There was further research into using Bayesian belief networks to help predict the development cost of software systems \cite{pendharkar2005probabilistic}.
The results of these experiments concluded that Bayesian networks allowed for several advantages when it came to predicting a real-world example, including the ability for managers to provide a probabilistic guess at what problems would likely be encountered in future development.

Subramanyam and Krishnan summarized works related to object oriented software metrics in their 2003 paper \cite{subramanyam2003empirical}.
They looked at papers published from 1993 through 2001, primarily focusing on CK-metrics (as defined by Chidamber and Kemerer \cite{chidamber1994metrics}).
And finally, they ran a case study on a large commercial system to identify a correlation between size, design metrics, and class-level defects.

Marinescu proposed searching for specific features within the code as a way of measuring complexity \cite{marinescu2005measurement}.
For example, he attempted to detect "god classes", and claimed that they would be identified by looking for classes with low coupling, high cohesion, and manageable complexity.

In the 2000's we also start to see the appearance of the term "model-driven engineering" \cite{balsamo2004model} \cite{france2007model}.
Although not directly tired to the software complexity as we've been using it, this development methodology calls for using tools to generate a high-level overview of the software package that allows designers to more easily identify patterns that they will be implementing.
Most of these tool implement the UML standard to allow designers to clearly lay out their programs, and then generate target code which can be further customized to their needs.


\subsection{The Present}

%\cite{fenton2014software}

Surprisingly, there have been new complexity metrics proposed in recent years.
One of the most recent was proposed by Tao in 2015 \cite{tao2014complexity}.
This metric takes a novel approach not seen by other measures we've looked at, but has not been examined critically.
It is evaluated against Weyuker's requirements for a good software complexity metric, but fails to do a rigorous case study against an industry example.
This does not mean the metric isn't good, just that, like many other metrics, additional evaluation is needed.

In recent years, Agile development methodologies have grown in acceptance and use within industry.
These technologies rely on the teams ability to quickly generate code, and then be able to go back and refactor it when needed.
Cinn{\'e}ide, Mat et al examined how several object-oriented metrics coincide with the locations of code to be refactored by examining a number of real world applications \cite{o2012experimental}.
They attempt to asses how well metrics evaluate the system, and whether or not different metrics that measure the same features of the system will produce similar results.
The results of their examination are grim; a large number of metrics will produce conflicting results.

In 2014, a very interesting paper that attempts to find a relationship between some complexity metrics (lines of code, cyclomatic complexity, and man-hours as estimated by the number of contributors) was published \cite{kochhar2014empirical}.
Kochhar et al established that in general, code coverage for open source projects based on Maven was 41.96\%.
They claim this is a low amount of test coverage, and while I agree it is less than ideal, I still find the number to very optimistic.
Something to consider with this study is the limitation that they were examining only projects which used Maven; Maven is a build automation tool that would primarily be used by professional developers who have industry experience, and not so much by academic or novice programmers.
This limiting nature, on the one hand, may give us a better cross-section of real projects, as opposed to abandoned, single developer projects, but it might also bias us towards a higher score than we would realistically expect.

In parallel to software complexity measures, there has been a steady stream of work that seeks to identify and quantify broader measures of complexity for any system: metabolic networks, neurons, ecosystems, social networks, and so on.
Mitchel summarizes this work very well in her 2009 book \cite{mitchell2009complexity}.
She points out that despite the high level of mathematical sophistication and extraordinary institutional support, no single universal metric has been found. Different measures of complexity seem to be a fundamental part of the field, and some measures may be better for different applications. But biological processes tend to stand out as both the system to be analyzed, and the inspiration for the measure. 
This is what leads us to believe that by examining biological analogs, we will be able to gain insight that has thus far eluded the software engineering community.
% Use of program features to determine complexity
% -> Fractal Dimensions
% -> Nesting of loops
% -> Graph complexity measures
% --> Control flow graph
% --> Cyclomatic graph
% --> Variable intersection graph?
% --> OOP UML diagrams

% Do any control flow graph graphs have a power-law distribution
% 15 metrics in a table
%  When, Why, Class, Difficulty to evaluate

\subsection{Classifying Software Metrics}
\label{chap:classifying_software_metrics}

Finally, this section attempts to classify the metrics and publications that have been discussed in the last section. 
We create a summary of the metrics, as seen in figure \ref{metric-classes}, using classes gathered from the literature (such as Fenton's work, \cite{fenton2014software}) and from an observation of the metrics.

We are utilizing the following classes:
\begin{enumerate}
	\item CF-based: These metrics utilize a control flow graph which represents the structure of the program commands to calculate a metric \footnote[1]
	
	\item DF-based: These metrics are calculated by tracking the setting and unsetting of variables within the program; we also classify metrics that utilize the number of variables in this domain \footnote[1]{This class was borrowed, with slight modifications, from Fenton's work}
	\item Function-flow based: Metrics that exam the flow of function calls through the program; although this is similar to 1, it looks at more than individual functions
\end{enumerate}

Classes 1, 2, and 3 come from Fenton's work, with some modification.
Basili and Hutchens also proposed a number of metric classes \cite{basili1983empirical}.
They defined three primary classes: volume, control organization, and data organization.
These categories map fairly well onto the ones listed above; data organization being data structure based, control organization being part DF and part CF.
There is no clear mapping for volume, which is effectively the size of the program, but very few metrics aim to measure the size, instead using lines of code to represent that metric (Basili makes the argument that some metrics do this even if it's not their goal, yet fits most of the metrics they claim this for into the control organization class as well). McCabes, based on the control flow graph that
is used to calculate McCabes metric, but gives more weight to type of control
flow statement and their sequence. This paper isn't particularly pivotal, but
represents the thoughts of researchers at this time.


\begin{table}[!ht]
	\begin{center}
		\caption{Summary of Several Complexity Metrics}
		\begin{tabular}{|c|c c c|}
			\hline
			Name & When Created & Purpose & Class of Metric \\
			\hline
			McCabes \cite{mccabe1976complexity} & 1976 & Testing & 1 \\
			Halsteads \cite{halstead1977elements}  & 1977 & ----- & 2 \\
			Yin \cite{yin1978establishment} & 1978 & Quality & 3 \\
			Knot Measure \cite{woodward1979measure} & 1979 & Quality & 1 \\
			Dataflow \cite{oviedo1993control} & 1980 & Quality & 1, 2 \\
			Information Flow\cite{henry1981software} & 1981 & Quality & 3 \\
			Prather's $\mu$ \cite{prather1984axiomatic} & 1984 & Testing & 1 \\
			Tree Impurity \cite{ince1988approach} & 1988 & Tool & 3 \\
			NPath \cite{nejmeh1988npath} & 1988 & Testing & 1 \\
			Program Slicing \cite{tao2014complexity} & 2014 & Quality & 1\\
			\hline
		\end{tabular}
	\end{center}
	\label{metric-classes}
\end{table}

In addition to classifying how these metrics work, we are also interested in the reason given by authors for their creation.
To this extent, we propose the following categories:
\begin{enumerate}
	\item Testing - Fenton and Bieman claim that the reason for developing this measure is to assist in testing the product; they do not mention the word "quality"
	\item Quality - Fenton and Bieman claim that the reason for developing this measure is to help interpret  program quality; the paper may mention testing
	\item Tool - Fenton and Bieman talk about a tool created using this metric
\end{enumerate}
% Expirement idea:
% - Given a project with VCS and a list of bugs/features
%  step through it bit by bit and find which portions of code were fixed
%  for each bug
% - Try to apply a huge variety of metrics to each portion and then use
%  a ML technique to identify which metrics forecast the need for change
% \section{Documentation availability vs complexity}

% Tools for combining code with documentation
% -> docstringsgopher 
% -> iPython

% Spaghetti code vs Elegant code
% -> Graph SC vs EC on x-axis and Documentation vs No Documentation on y-axis

% Computers and Typesettings - simple idea, complex implementation

% Do the number of text books represent complexity of topic, in a generalized setting?

\section{Complexity in Industry}

% Complexity vs Fault Rate
The use of complexity metrics in industry goes back to the founding of the field.
Some of the first goals of the field was to provide engineers with a way of estimating the cost of maintaining source code over a long period of time, predicting where bugs would appear in the source code, and measuring programmer productivity.

The idea of using software metrics as a way of predicting the cost of maintenance of systems has some success.
Researches such as Banker \cite{banker1993software} and Kafura \cite{kafura1987use} have found positive correlations between complexity metrics and maintenance cost.
However, there is little evidence that the results of these works is applying to real-world scenarios.

One of the more recent papers related to predicting fault rate by analyzing the complexity of code was done by Hassan, in 2009 \cite{hassan2009predicting}.
In addition to looking at the complexity of the code, Hassan also accounted for the way time impacted the changes; he attempted to find a correlation between the complexity of a specific change (or series of changes) as recorded by a version control history and the number of faults that would be found in the that part of the code.
Hassan finds that using this method he can predict fault more accurately than either merely looking at previous faults in a module or looking at the complexity of code changes alone.

An interesting application related to industry use of complexity metrics may be to measure technical debt accumulated in different modules.
In their abstract, Brown et al do a good job of summarizing technical debt:
\begin{quotation}
	The idea is that developers sometimes accept compromises in a system in one 	dimension (e.g., modularity) to meet an urgent demand in some  other dimension (e.g., a deadline), and that such compromises incur a "debt": on which "interest" has to be paid and which the "principal" should be repaid at some point for the long-term  health of the project \cite{brown2010managing}.
\end{quotation}
Determining where these compromises were made, and how many total compromises were made, could help project managers triage the compromises: a novel use of software complexity metrics.

One of the current major issues in the software engineering community is ensuring application security and preventing information leakage.
To this end, Huang, Dong, and Milanova have done recent work to develop an application that can trace information through Java applications \cite{huang2015scalable}.
If we can find a correlation between complexity and information leakage, this may help to provide a way to focus SFLow software and rule out additional false positives.
This correlation may be negative; that is, programmers are more likely to not notice information leaking in a trivial aspect of the code due to negligence, whereas they will pay more attention on more complicated pieces of code.

We explore the use of complexity metrics in industry more in chapters \ref{interactive_complexity}, \ref{taint_analysis}, and \ref{machine_learning}.

% Relationship to Taint Analysis?

% Million eyes; how many modify

\section{Conclusion}

This chapter covered many metrics, including a brief discourse into the connection to the domain of complexity as defined by Mitchell et al.
The differences between the metrics identified were discussed in section \ref{chap:classifying_software_metrics}, and the metrics grouped.
Finally, we talked about a number of ways in which software complexity has been applied.

In the next few chapters, we expand on the applications of the metrics dicussed here, and contrast them to metrics proposed in the chapter \ref{interactive_complexity}.
The top-down approach used by a majority of these metrics has not been particularly successful in finding a way to gain adoption by large swaths of the programming community.
Although a few applications exist, most of the recent work in managing software complexity has come about in the form of static analysis tools, such as JSlint and clang-analyzer.
This approach doesn't match perfectly with what the goal of many software complexities; where tools like clang-analyzer aim to detect specific problems within a given program, the complexity metrics discussed here aim to provide feedback to the programmer about whether their code will be difficult to maintain, difficult to understand, or difficult to debug.

The use of metrics to measure student comprehension, discussed in chapter \ref{icedutech}, is a novel application of complexity metrics.
Many automated grading tools measure student understanding by looking at the end product, or checking for specific features.
For example, the grading tool is given a set of unit tests, and the student submission is graded by their ability to get correct answers for the given tests.
Getting a sense of student code clarity is still difficult, and although many rubrics include a score for this, it is still a manual process subject to review interpretation.
