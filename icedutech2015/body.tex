\section{Introduction}

% What is open source? very brief regurgitation
% Why do we care about open source? list large projects that "matter"
% --> Include projects listed as "can not fail" internet services
% Does it matter how open projects are?
% How will being able to measure how open projects are help organize things

Understanding how much code changes from one point to the next is a fundamental question in many domains, including the assessment of: students'progress in learning; originality in legal domains; documenting the contributions of laborors; and even tracking biological material, if we think of genetics or amino acid sequences as code. Complexity is one metric by which code changes over time might be monitored. Take, for example, the increasing evidence that womens' contributions are undervalued in the software community \cite{freebsd2015codeofconduct}. If we can show that these undervalued changes by female coders are just as complex as those of males, it would provide one way to document that bias, and perhaps encourage the celebration of the contributions that normally go unappreciated.

For simplicity, this thesis will obtain its code samples from a visual programming environment created for the 3Helix program at Rensselaer Polytechnic Institute, called "CSnap". In 2014 the author of this dissertation created an Django-based online community by which CSnap scripts could be uploaded, modified and shared by a community of learners; as of this writing there are over 5,000 projects in its database. Our vision was that we could track these scripts as they "evolved" over time. Scripts which won this Darwinian competition might help point us in the direction of better educational practices; the evolution could perhaps be "directed" so that more beneficial versions are less likely to die out; and other analogies with biological mutation, evolution, and population dynamics could be explored. 

The project could have filled an entire dissertation by itself. Since the goal was to investigate this only at the proof of concept level, we decided to run a small-scale experiment on 5 people, asking them to rank 5 projects (which produce similar output) on how complex they appear to be. These subjective judgements could then be compared to objective software complexity metrics. This would help us see how a large-scale, statistically valid experiment of this sort might be run in the future, eventually leading to a validated metric for the ability of students to produce more or less complicated scripts.  

On the objective side, in addition to using more typical software complexity metrics (McCabe \cite{mccabe1976complexity}, Oviedo \cite{oviedo1993control}, and Halstead \cite{halstead1977elements}) we decided to add one of the metrics used by biologists when they measure rates of mutation in evolution; in our case Komolgorov complexity \cite{hernndez2017algorithmically} in the form of normalized compression distance.

Reproducing this experiment with a larger sample size will be required to gain true meaning, but this limited test provides us with insight to improve the process.
The tools developed as part of this thesis can be used to analyze projects stored on the CSDT Community, which can be combined with a survey to get this larger sample size and more insight into correlations with learning.



\section{Literature Review}

% We have at least 2 distinct "areas" to review; open source-ness, and ways of measuring software similiarity

As a form of literature review, we will use a number of well-known papers that represent some of the most important works in this field.
Most of these articles have been reprinted in \textbf{Software Engineering Metrics Volume 1} \cite{sheppherd1993complexity}, including:
\begin{itemize}
	\item \textbf{A Complexity Measure} \cite{mccabe1976complexity}
	\item \textbf{Control Flow, Data Flow, and Program Complexity} \cite{oviedo1993control}
\end{itemize}

It is worth mentioning that most of these metrics have had their accuracy questioned, and for the most part been proven to fail in one or more ways.
A very well known paper that described the failure of these metrics is \textbf{Evaluating Software Complexity Measures}, by Elaine J. Weyuker \cite{weyuker1988evaluating}.
Weyuker's work has been used as the core for many following software complexity papers, including the recent publication by Hongwei Tao, \textbf{Complexity measure based on program slicing and its validation} \cite{tao2014complexity}.

% More stuff?

\subsection{Evaluating Software Complexity Measures}

% We need to talk about everything, including silly things like...
% Lines of code changed

In this section, we will summarize and describe previous metrics, with particular attention to Weyuker's analysis.
For convenience, I have copied the 9 properties of complexity measures she proposed here. 
Note, however, that they are explained in greater detail in the paper.

In the following statements, P and Q represent entire programs.
In order to satisfy each property, the metric result of each of the proposed methods must satisfy all of these properties to be considered correct; the use of cardinality operators in this context returns the result of a particular metric algorithm instead of the length of a program.
% begin comment msk -
%You have to explain what P and Q are Are they Programs/Predicates -
%Intuition behind each of these properties have to be esplained
%% Addressed?
% end comment msk

\begin{itemize}
	\item Property 1: $(\exists P)(\exists Q)(|P|\neq |Q|)$
	\item Property 2: Let c be a nonnegative number. Then there
are only finitely many programs of complexity c.
	\item Property 3: There are distinct programs P and Q such
that $|P| ~= ~|Q|$
	\item Property 4: $(\exists P)(\exists Q)(P \equiv Q ~\& ~|P| \neq |Q|)$
	\item Property 5: $(\forall P)(\forall Q)(|P| \leq |P; Q| ~and ~|Q| \leq |P; Q|)$
	\item Property 6a: $(\exists P)(\exists Q)(\exists R)(|P| = |Q| \& |P;R| \neq |Q; R|)$
	\item Property 6b: $(\exists P)(\exists Q)(\exists R)(|P| = |Q| \& |R;P| \neq |R; Q|)$
	\item Property 7: There are program bodies P and Q such that Q is formed by permuting the order of the statements of P, and $|P| \neq |Q|$
	\item Property 8: If P is a renaming of Q, then $|P| = |Q|$
	\item Property 9: $(\exists P)(\exists Q)(|P|+|Q| < |P; Q|)$
\end{itemize}

\subsection{Summarization of Control Graphs}

Control graphs are used by many metrics, and rather than repeating their design in every section, I will describe them here. 
There has been some change in the nature of computer programming which makes many of the graphs demonstrated in older literature obsolete, namely the case where a GOTO drops execution into a loop or conditional from elsewhere.
We will set aside this case, to make the generation much more simple.

\subsection{Cyclomatic complexity}

Thomas McCabe proposed a complexity measure, cyclomatic complexity \cite{mccabe1976complexity}.
In this section, I will briefly describe how it works, how to calculate it, and discuss some responses to it (specifically the responses made by Elaine Weyuker in her paper \cite{weyuker1988evaluating}).

\subsubsection{How it works}

The McCabe metric (Cyclomatic number) analyses the control flow of a program to determine how complex it is.  
There are 3 primary items that is used the calculation of the cyclomatic number:
\begin{itemize} 
  \item Number of nodes, denoted as n
  \item Number of edges, denoted as e
  \item Number of compontents, denoted as p
\end{itemize}

Given these three variables, we can calculate a complexity ($\mu$) of a program using the following equation:

$\mu ~= ~e ~- ~n ~+ 2p$

It should be noted that we use 2p instead of p; some books use p, but draw an exra edge back to the start node from the end node in each component.
These two methods result in identical complexities, but I prefer the second because it makes it easier to identify the start and end nodes in my program.

The real challenge is to create the graph so you can find values for these variables.
To do this you must break a program into parts such that there is a starting node, and ending node, and a sequence of nodes between that two have the following key attributes: 
\begin{itemize}
  \item A compound statement is only one node; ie, int i=0; i = b*x; ... counts as only one node until the next branch occurs
  \item A branch is a conditional, either in the form of an if statement or a loop (with a loop, connect the node to both the following statement(s) and the code that gets executed after it exits)
\end{itemize}

As a general rule of a thumb, we have a component anywhere a loop occurs.
We can have multiple components inside a component (embedded loops).

Once we have created a graph, it is a trivial matter of counting the number of edges, nodes, and components and applying the equation.

\section{Analysis}

This metric was included in the analysis performed by Weyuker in her paper \cite{weyuker1988evaluating}.
Ultimately, she concluded that McCabes metric failed to address properties 2, 6, 7, and 9.

\subsection{Normalized Compression Distance}

Proposed by Rudi Cilibrasi and Paul Vitanyi, \textbf{Clustering by compression} \cite{cilibrasi2005clustering} puts forth a very straightforward way of calculating complexity changes between two pieces of code, without even the knowledge of program structure or content.
They envisioned this method being used to cluster data for machine learning.
However, we believe it may also be a good way to measure change between two programs; if the distance between a start point and an end point is large, there is a great deal of change.
If the distance is not significant, there is not a significant amount of change.
% Add references for gzip, PPMZ, Kolmogorov
They proposed using a standard compression method (such as gzip or PPMZ) to approximate the Kolmogorov complexity of a program, and using this solve the equation in equation \ref{ncd_eq_1}.

\begin{equation}
	\label{ncd_eq_1}
	\centering
	NCD(x,y) ~= \frac{C(xy) - min(C(x),C(y))}{max(C(x),C(y))}
\end{equation}

\subsection{Analysis}

Weyuker did not analyze this metric in her paper, and as of the time of writing this thesis, no one has attempted that comparison.
This is may be due to the fact that we don't normally analyze the distance between two sets of code as a metric, even though many of Weyuker's metrics are entirely about finding a change between two pieces of code.

In this section, we compare primarily programs P and Q.
Rather than do something questionable with comparing them to each other, we want to find a way to compare them directly.
After some consideration, we have decided to simply look at the C(x) for the purposes of evaluating Weyuker's properties.
The reason for this is that we want to compare NCD, but want to do it with single programs.
Since NCD is a function of C, one could imagine that NCD has the same properties as C.

And so, without further ado, we provide a brief pass over all of Weyuker's properties with NCD in mind.
We will not provide formal proofs, but we will give an explanation as to why each is or isn't satisfied. 

\begin{itemize}
	\item Property 1: $(\exists P)(\exists Q)(|P|\neq |Q|)$
	\begin{itemize}
		\item This can easily be demonstrated; in the examples we give in the appendix, there are 4 programs, all of which have different complexity measures.
	\end{itemize}
	\item Property 2: Let c be a nonnegative number. Then there are only finitely many programs of complexity c. Note that c is not C; little c is a number, and big C represents a compression method.
	\begin{itemize}
		\item Given that for NCD a program is just a compression of source code, we could expect a program with contents 'x+=1;' to be smaller than a program with contents 'x+=1;y+=1'.
	\end{itemize}
	\item Property 3: There are distinct programs P and Q such that $|P| ~= ~|Q|$
	\begin{itemize}
		\item This is harder to envision, but if we had two programs that were the same code moved around, then compressed, we would get the same compression size. Take a program (in some hypothetical language) that consisted of the code "x+=1;y+=1", and its brother program that consisted of "y+=1;x+=1". Compressing these programs results in a compression size of 34 for both.
	\end{itemize}
	\item Property 4: $(\exists P)(\exists Q)(P \equiv Q ~\& ~|P| \neq |Q|)$
	\begin{itemize}
		\item As long as the two programs aren't identical, and aren't permutations of each other, we could imagine two programs with different NCD's and the same output. For example, if we added statements that had no effects on the output.
	\end{itemize}
	\item Property 5: $(\forall P)(\forall Q)(|P| \leq |P; Q| ~and ~|Q| \leq |P; Q|)$
	\begin{itemize}
		\item Although in our examples it \textit{may} be possible to prove this one wrong, this is most likely due to our not-quite-perfect compression; one could imagine that with a perfect compression, you could get a metric that is the same size, but not smaller. \footnote{Note that we are ignoring cases where two operations, when taken together, can be simplified to a single (or no) operation; our goal is to evaluate program complexity, not whether or not it is optimal.}
	\end{itemize}
	\item Property 6a: $(\exists P)(\exists Q)(\exists R)(|P| = |Q| \& |P; R| \neq |Q; R|)$
	\item Property 6b: $(\exists P)(\exists Q)(\exists R)(|P| = |Q| \& |R; P| \neq |R; Q|)$
	\begin{itemize}
		\item NCD will account for this; if program R has subsets of program Q in it, C(Q, R) would result in a different size than C(R, Q).
	\end{itemize}
	\item Property 7: There are program P and Q such that Q is formed by permuting the order of the statements of P, and $|P| \neq |Q|$
	\begin{itemize}
		\item One can imagine a program consisting of repeated "a"'s an "b"'s 50 times each. If we arrange a program where 50 a's follow 50 b's, our compression will be very good. If we take this same program, but move one b to the front of the program, our compression will be slightly larger (by the space it takes to indicate that there is 1 b in the front). i.e.; program A is (50a50b), which our compressor expands to 50 a's followed by 50 b's. Program B is (1b50a49b) which our compressor expands to 1 b, 50 a's, 49 b's. Program A is 4 bytes large when compressed, program b is 6 bytes large. 
	\end{itemize}
	\item Property 8: If P is a renaming of Q, then $|P| = |Q|$
	\begin{itemize}
		\item NCD would fail in this respect; longer variable names would result in longer compressed scripts
        %% Add example of variable name of x<pi>; something else that doesn't compress well
	\end{itemize}
	\item Property 9: $(\exists P)(\exists Q)(|P|+|Q| < |P; Q|)$
	\begin{itemize}
		\item It is unlikely this would be true for NCD; adding two programs together gives the compressor more to work with, and therefore it would be able to do a better job. Just adding the size of the smaller compressed files would fail to work (just think about the overhead we have to add in! Program "ab" went from size 3 to size 29 when we compressed it). It should be noted that we may be able to get $|P|+|Q|=|P; Q|$ if we had 2 programs that can't be compressed; but equal is not less than.
	\end{itemize}
\end{itemize}

In summary, using compression as a metric of complexity fails for properties 8 and 9.
In comparison to the other metrics Weyuker evaluated, this has the same number of problems as the effort measure, and data flow complexity, but fewer problems than lines of code, and cyclomatic number.

%\subsection{Effort measure}
% Forgo this section for now, I'm missing the book where it is defined
% Need to trace this back I think
% This is sort of the thing that Ron's expirement would look at
%\cite{weyuker1988evaluating}

\subsection{Data flow complexity}

Oviedo proposed a metric which relies on 2 key components; data flow (DF) and control flow (CF) \cite{oviedo1993control}.
He concludes with the equation $C = \alpha F_C + \beta F_D$ (where $\alpha=\beta=1$, C is the complexity).
% This later followed up by ... who said \alpha should equal and \beta should equal...
%% CF of F_C?

%begin comment msk
% You need to give an example here - The notations are not known in the compiler area 
%end comment msk

\subsection{Analysis}

This metric was included in the analysis performed by Weyuker in her 1988 paper \cite{weyuker1988evaluating}.
Ultimately, she concluded that the Dataflow metric failed to address properties 2 and 5.

\subsection{Other metrics}

In addition to the above stated metrics, we will also consider lines of code (also referred to as statement count).
This metric is almost universally accepted as not being indicative of the complexity of a program, and will provide a sane reference point to verify that we are not just finding correct data.

Lines of code is very dependent on the language in which we are evaluating programs.
Since we are using CSnap as a small-scale staging run, we will consider a "line of code" to be a command block (as defined by Snap! and CSnap).
These blocks include things such as "if", "go to {x coordinate}", etc.

\section{Measuring change}

% We really need to decide what to do about this :(

% Propose new metric in this paper and test with the toy example (small)

% Most likely an aggregate metric, but what weights and metrics?

There are several issues that need to be addressed by our algorithm if we are to effectively measure change in software over a large period of time.
These include:
\begin{itemize}
	\item Accounting for "non-changes"; i.e. making a change to a project, testing it, then changing it back
	\item Discerning between changes that required little to no effort, and changes that may only have been to a line or two, but required a great deal of effort
	\item Considering things that change the output as part of the metric; not all changes will change the end-result of a program, but will rather optimize the process; other changes will cause the behavior of the program to dramatically alter. How do we differentiate between these things?
\end{itemize}

The first item is pretty easy to account for; if we make a change, we would expect that undoing the change would result in the same (but opposite) delta as the original change.
The other two items are much more difficult to measure.

Determining how much effort a change has required is analogous to an employer asking "How productive has my employee been?", and that is a question that still remains unanswered; papers on the subject have been published as recently as 2015 \cite{phusavat2015use}.
Therefore, in this thesis, we will instead focus on simply measuring the effort represented by the complexity of generated code; it won't take into account time spent designing or researching the solutions that are applied.

This has the side effect of making several measures somewhat questionable; notably the data-flow complexity measure which accounts for a certain amount of research by tracking the distance between where a variable is used and where it was defined.
What we mean by research in this chapter is exploring third-party libraries, examining core dumps, tracing program flow, etc.
The research that is accounted for by data-flow is strictly within the corpus that we are analyzing, and therefore differs from research that would happen outside of the scope of what we can measure within the codebase.

Given these constraints, the problem becomes a subset of the larger domain of software engineering, and not economics or business.
We need to know very specifically how the code changes, and how much complexity each change represents.
To do this, we will examine the metrics described in the previous section, and apply them to real-life scenarios to determine which is the most appropriate for measuring the complexity of algorithms.
This type of empirical research has very few direct ancestors; there are publications that focus on the application of software complexity to defect count, from the perspective of measuring test effectiveness \cite{inozemtseva2014coverage} \cite{persson2015correlation}, but very few that attempt to quantify the openness of open source software by measuring code complexity.

\section{Experiment proposals}

% Ron's proposal goes in here;
% As a remind for the me that forgets later;
% --> Design a base CSnap project
% --> Modify the project to target a specific metric
% ---> This will be done multiple times; 1 project for each metric, then combinations
% --> Ask "people" which projects represent the most change, the least
% --> See which metrics agree with the responses the most

Having defined our complexity measures, we now turn to the small scale experiment in the domain of education and student progress. As noted previously, the ultimate vision is to engage teachers in developing some meaningful relations between their evaluation of student work and complexity metrics, using statistically valid samples. At this point we only wanted to try some initial steps in that direction, so we limited ourselves to 5 scripts, and 5 instructors who could provide their subjective judgements of the script complexity. Because one of the objectives is to investigate its utility for diversity purposes, we decided to include questions about identity as well.

\section{Pre-experiment Results}

For this first draft, we created a survey consisting of the following questions
\begin{enumerate}
	\item Please rank the above projects from most complex (5) to least complex (1)
	\item How complex would you rate each project (1 = not very complex, 100 = very complex)
	\item What is your gender?
	\item Which race/ethnicity best describes you? (Please choose only one.)
	\item What was your major?
\end{enumerate}

The scripts presented to the subjects can be seen in figures \ref{alpha}, \ref{beta}, \ref{bravo}, \ref{charlie}, and \ref{delta}

\begin{table}[h!]
	\caption{Calculate Metrics for Scripts}
	\label{calculate_metrics}
	\centering
	\begin{tabular}{| l | c | c | c | c | c |}
		\hline
		& Cyclomatic & Dataflow & NCD & Lines of Code \\
		\hline
		Alpha & 1 & 2 & 0.054 & 23 \\
		Beta & 2 & 6 & 0.031 & 14 \\
		Bravo & 1 & 2 & 0.039 & 11 \\
		Charlie & 6 & 27 & 0.056 & 25 \\
		Delta & 7 & 23 & 0.06 & 19 \\
		\hline
	\end{tabular}
\end{table}

\subsection{Program Output}

Figure \ref{calculate_metrics} shows the calculated metrics of each of the scripts.
These metrics were calculated via a program which will be included with the followup work to this thesis, or when requested.
NCD in this table represents a comparison between the given project, and Alpha. 
We used Alpha as a based project, and calculated the NCD in relation to that project.
Interestingly, for 2 identical programs we ended with a wildly different NCD; this is due to the overhead introduced by the compressor used.

\subsection{Survey Results}

We had a total of 5 people complete the survey, and obtained the results listed in figures \ref{survey-results-relative} and \ref{survey-results-absolute}.

\begin{table}[h!]
	\caption{Survey Results -- Relative}
	\label{survey-results-relative}
	\centering

\begin{adjustbox}{max width=\columnwidth}
	\begin{tabular}{|l|l|l|l|l|l|}
			\hline
Respondent & rel\_alpha & rel\_beta & rel\_bravo & rel\_charlie & rel\_delta \\
		\hline
1          & 3          & 1         & 2          & 5            & 4          \\
2          & 1          & 4         & 3          & 5            & 2          \\
3          & 2          & 3         & 1          & 4            & 5          \\
4          & 1          & 5         & 2          & 4            & 3          \\
           &            &           &            &              &            \\
Mode       & 1          & \#N/A     & 2          & 5            & \#N/A      \\
Median     & 1.5        & 3.5       & 2          & 4.5          & 3.5        \\
Mean       & 1.75       & 3.25      & 2          & 4.5          & 3.5       \\
		\hline
\end{tabular}
\end{adjustbox}
\end{table}
\begin{table}[h!]
	\caption{Survey Results -- Absolute}
	\label{survey-results-absolute}
	\centering
\begin{adjustbox}{max width=\columnwidth}
	\begin{tabular}{|l|l|l|l|l|l|}
			\hline
Respondent & abs\_alpha & abs\_beta & abs\_bravo & abs\_charlie & abs\_delta \\
		\hline
1          & 30         & 10        & 15         & 50           & 45         \\
2          & 5          & 13        & 11         & 20           & 10         \\
3          & 16         & 32        & 8          & 64           & 100        \\
4          & 20         & 50        & 25         & 40           & 35         \\
           &            &           &            &              &            \\
Mode       & \#N/A      & \#N/A     & \#N/A      & \#N/A        & \#N/A      \\
Median     & 18         & 22.5      & 13         & 45           & 40         \\
Mean       & 17.75      & 26.25     & 14.75      & 43.5         & 47.5      \\
		\hline
\end{tabular}
\end{adjustbox}
\end{table}


\begin{table}[h!]
	\caption{Survey Results -- Responder Gender and Ethnicity}
	\label{survey-results-gender}
	\centering
\begin{adjustbox}{max width=\columnwidth}
\begin{tabular}{|l|l|l|}
		\hline
Respondent & gender & ethnicity  \\
		\hline
1          & male   & white   \\
2          & female & white  \\
3          & male   & white   \\
4          & female & african american  \\
           		\hline
\end{tabular}
\end{adjustbox}
\end{table}

\section{Conclusion}

We can clearly see a consensus that sample program charlie (figure \ref{charlie}) is the most complex project, and sample program alpha (figure \ref{alpha}) the least complex.
There is some disagreement as to how complex delta (figure \ref{delta}) was; 3 of the respondents listed it as fairly complex (and in most cases, much more complex than the less complex project), except for respondent 2.
However, this respondent commented that the reason for the lower ranking was that it didn't rely on "hidden magic" in the draw triangle block.
This raises an interesting discussion around our program, which did not delve into the blocks when calculating complexity.
By not evaluating hidden blocks, we can focus in on the code the user sees, which we believe to be more important in terms of measuring user contribution (but might lead to inaccurate data-flow results).

If we take each project's relative score from each respondent and sum them, we can order projects (from least to most complex):
\begin{enumerate}
	\item Bravo (7)
	\item Alpha (8)
	\item Beta (13)
	\item Delta (14)
	\item Charlie (18)
\end{enumerate}

% Bravo, Beta, Delta, Alpha, Charlie
There is only a very small correlation with Lines of Code as a metric; 2/5 scores line up.

% alpha, bravo, beta, delta, charlie
Cyclomatic complexity and data flow are much better; 5/5 (alpha and bravo tied, but they were also really close according to user ratings).
This suggests that these two ratings are more accurate than LOC (lines of code).

Comparing to NCD (normalized compression distance) is a bit more tricky, as we need to see how similar they are.
There is no accurate way to do this based on the survey, and would require a much larger testing group as it would be much more subjective.
This metric did not perform as well on this small experiment as the other metrics, suggesting that the domain-specific complexity metrics have an advantage in this application.
Given that the goal to determine if students can think computational, which usually equates to understanding iterative thinking and conditional branching, it does not seem surprising that metrics which specifically look for these features in the application perform better in this case.

The absolute ranking provides an interesting insight if one looks at them as functions of "distance".
Most users didn't utilize the entire range (1-100), but instead stuck with a range of 1-50 or less.
We can say that, on average, the least complex project (according to the relative ranking, bravo), is 3.5 times less complex than the most complex project (charlie).
This may be skewed by responder bias; we can clearly see that responders who went up to 100 would have gladly gone up to 128 if we had let them.

\subsection{Conclusion}

Based on this small sample size, no correlations would be statistically significant.
However, it does help to clarify the problem, and demonstrate how a larger experiment may be conducted.

A metric that can provide an objective basis to examine differences in subjective assessments of software might be useful in addressing issues such as gender equity.
For example, Eskowitz et al \cite{etzkowitz1992athena} note that female faculty in science tend to have fewer publications, but that the few have higher citation rates, and that this creates a gender bias when tenure committees are looking more at quantity than quality.
They recommend that academics devise a more universal metric that would validate both styles.
A similar gender difference in work styles is described by Turkel and Papert \cite{turkle1992epistemological}, who suggest that a male/female difference can be found in comparing "top-down" vs "bottom-up" code development styles: they suggest that this creates a challenge for assessment which tends to have a subjective bias towards top-down (just as evaluators in Eskowitz et al found a bias towards quantity over quality).
Thus automated analysis of code complexity could be a tool for helping evaluators avoid subjective bias.

With that in mind, there was an intriguing correlation: delta was the only project that did not use code that was "hidden" in blocks, and the two female participants ranked delta lower than the males had ranked it.
Again the numbers here are too small for statistical significance, but as we move this system to larger numbers, we will continue to look for such patterns.

\section{Programs and control flow graphs}

\begin{figure}[H]
	\label{alpha}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/alpha.png}
	\caption{Sample Project Alpha \\ Cyclomatic 1, Dataflow 2, LOC 23}
\end{figure}

\begin{figure}[H]
	\label{beta}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/beta.png}
	\caption{Sample Project Beta \\ Cyclomatic 2, Dataflow 6, LOC 14}
\end{figure}

\begin{figure}[H]
	\label{beta_graph}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/beta_0.png}
	\caption{Sample Project Beta}
\end{figure}

\begin{figure}[H]
	\label{bravo}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/bravo.png}
	\caption{Sample Project Bravo \\ Cyclomatic 1, Dataflow 2, LOC 11}
\end{figure}

%% TODO Fix height issues with these
\begin{figure}[H]
	\label{charlie}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/charlie.png}
	\caption{Sample Project Charlie \\ Cyclomatic 6, Dataflow 27, LOC 25}
\end{figure}

\begin{figure}[H]
	\label{charlie_0}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/charlie_0.png}
	\caption{Sample Project Charlie Cyclomatic Graph}
\end{figure}

\begin{figure}[H]
	\label{delta}
	\centering
	\includegraphics[width=\columnwidth]{./icedutech2015/sample_csnap_applications/delta.png}
	\caption{Sample Project Delta \\ Cyclomatic 7, Dataflow 23, LOC 19}
\end{figure}

\begin{figure}[H]
	\label{delta_0}
	\centering
	\includegraphics[height=\dimexpr\textheight\relax]{./icedutech2015/sample_csnap_applications/delta_0.png}
	\caption{Sample Project Delta Cyclomatic Graph \\ An interesting aspect of the graph is embedded for loops; these do not add more complexity than 2 sequential loops, but produce a much more complicated looking graph}
\end{figure}
