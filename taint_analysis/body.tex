\section{Introduction}

% Objective: we tried using complexity metrics for something their not good at, it went poorly.

Using complexity metrics to gain meaningful insight into an application has been with very limited scope.
The primary objective in most uses of conventional software complexity metrics is to predict maintenance cost; while this is a good objective, it is hardly the only reason massive effort is put into analyzing software programs.
In this chapter, we explore the use of conventional complexity metrics in a task more suited to what they were designed for; predicting something about the software they are being applied too.

As devices become more sophisticated they gain access to more information that we utilize everyday; information that can be used for nefarious purposes, if we are not aware of the usage.
% Cite XKCD above
Some of the most sophisticated devices we have, more complex than a multi-thousand dollar computers, we carry around with us everyday; cell phones.
These devices have access all kinds of information; our location, contacts, they can even (potentially!) record video and audio without our knowledge.

To help us cope with massive amount of information these devices can gather, the developers provide ways of limiting what applications on the phone have access to these various pieces of information; why would my text messaging app need access to  my location? Why would my contact list need access to camera?

Even with these tools however, there is still huge risks that we need to address.
For example, I would not be alarmed if my "camera" app needed access to my camera.
However, I would be alarmed if I found that app uploading my pictures to some location in the cloud that I can't control.
Similarly, I would not be surprised if my navigation app needs access to my location, but does it really need to tell big brother where I am?
Finding what these apps are doing with the data they gather is essential to protecting our freedoms, but it is a difficult task to accomplish.

Recent work in this field is theoretically intriguing but so far limited in practical application.
Huang et al. have been using static type inference to follow sensitive pieces of information from their sources, to places where they may be at risk of leaving the system and being given to another resource, on the internet or within the phone (log files or other applications) \cite{huang2015scalable}.
They found that by itself, their DroidInfer system had an excellent detection rate, comparable to the state-of-the art, but still had several false positives (79\% precision).
We suspect this issue of false positives is a large contributor to why techniques like those used by Huang don't often get used in industry.

This chapter focuses on the use of the CK-metric suite (and a few other metrics) to predict whether a detection rate is a false positive or not.
In essence, the use of these metrics hopes to differentiate between programming errors, and intentional information leakage.

\section{Methodology}

In a previous publication Huang et al described the dataset they analyzed with their DroidInfer application \cite{huang2015scalable}.
In this chapter, we take those apps and analyze them using our Software Metrics Framework (SMF) to calculate a variety of software metrics, all previously published and well known, then analyze the data for patterns and correlations.

The Software Metrics Framework is built off of the Soot Framework, which has been used to do various studies related to static type analysis, including the work done with DroidInfer.
Currently, the SMF consists of a small subset of all possible metrics; there are hundreds of them, some of which are substantially easier to programmatically calculate than others.
The included metrics are described in more detail below.


\section{Included Metrics}

We included metrics from the 3 classes described in section \ref{chap:classifying_software_metrics}; control-flow based metrics such as Cyclomatic Complexity, data-flow based metrics such as Halstead's Metric, and function-flow based metrics, such as the CK-Metrics suite and Information Flow.

\begin{enumerate}
	\item Cyclomatic
	\item Data Flow
	\item Entropy Measure
	\begin{enumerate}
		\item For the purposes of this thesis, we use the difference between file-size and the gzipped size of the classes.dex from each APK; this should give us a good approximation of Kolmogorov complexity \footnote{previous works have used this technique}
	\end{enumerate}
	\item CK-Metrics
	\begin{enumerate}
		\item Depth of Inheritance Tree
		\item Coupling Between Objects
		\item Weighted Methods per Class
		\item Response Set For Class
		\item LCOM1 (we did not include additional LCOM metrics as they were not part of the original work done by the authors of the CK-Metric suite)
	\end{enumerate}
\end{enumerate}

In the next few sections we elaborate on both the definitions and implementations of the above metrics.
Many of these metrics are discussed in section \ref{lit_review}, but we delve more into detail here.

\subsection{Cyclomatic Complexity}

The McCabe metric (Cyclomatic number) analyses the control flow of a program to determine how complex it is. 
There are 3 primary items that is used the calculation of the cyclomatic number:
\begin{itemize}
	\item Number of nodes, denoted as n
	\item Number of edges, denoted as e
	\item Number of compontents, denoted as p
\end{itemize}

Given these three variables, we can calculate a complexity ($\mu$) of a program using the following equation:

$\mu ~= ~e ~- ~n ~+ 2p$

It should be noted that we use 2p instead of p; some books use p, but draw an exra edge back to the start node from the end node in each component.
These two methods result in identical complexities, but I prefer the second because it makes it easier to identify the start and end nodes in my program.

The real challenge is to create the graph so you can find values for these variables.
To do this you must break a program into parts such that there is a starting node, and ending node, and a sequence of nodes between that two have the following key attributes:
\begin{itemize}
	\item A compound statement is only one node; ie, int i=0; i = b*x; ... counts as only one node until the next branch occurs
	\item A branch is a conditional, either in the form of an if statement or a loop (with a loop, connect the node to both the following statement(s) and the code that gets executed after it exits)
\end{itemize}

As a general rule of a thumb, we have a component anywhere a loop occurs.
We can have multiple components inside a component (embedded loops). 

Once we have created a graph, it is a trivial matter of counting the number of edges, nodes, and components and applying the equation.
It should also be noted that having each node in the control-flow graph being exactly one statement is the same as grouping them together; each node without a branch will add exactly one branch and one node, so it won't have an effect.
There is also a well-known shortcut for programs without exception handling; the Cyclomatic Complexity is equal to the number of branching statements within a program.
We opted to calculate it the hard-way to account for case statements, exception handling, etc. 

As a concluding note on this metric, we reiterate the analysis done by Weyuker in her 1988 paper  \cite{weyuker1988evaluating}.
We briefly mentioned these findings in section \ref{icedutech}, but due to the nature of that publication did not expand on why this metric succeeded or failed her criteria.
A description of these properties can be found in section \ref{icedutech}.

Weyuker concluded that McCabes metric failed to address properties 2, 6, 7, and 9, for the following reasons:
\begin{itemize}
	\item Property 2: We can easily imagine an infinite number of programs that don't branch, but are just repeats of the same statement over and over (x+=2;x+=2;x+=2..). These programs would have a complexity of 1, and there are an infinite number of them
	\item Property 6: Since McCabes complexity measure doesn't account for the interaction of variables, appending a program (R) to the end of another program (P) would always result in $|P|+|R|$, whichs means that if $|Q|=|P|$, then $|Q|+|R| ~= ~|P|+|R|$.
	\item Property 7: The order of statements in McCabe's metric have no influence on complexity; we will always have the same number of branches, same number of nodes, and components
	\item Property 9: McCabe's metric fails this for the same reason as property 6
\end{itemize}

\subsection{Data Flow}

Control flow is simply the cardinality of the program flow graph.
The program flow graph, and how to construct it, is clearly defined in the paper.
It more or less follows the same structure as the graphs we construct for the Cyclomatic number, with a more formal definition being given to how a "program" (function in modern terminology) is denoted.
Formally,

$CF=\parallel E\parallel$

Where $\parallel ~\parallel$ stands for set cardinality.

The more complicated part of this metric is the data flow measure.
The key terminology for this measure is the distinction between a locally available variable and a locally exposed variable.
A variable is locally available when the variable is defined in the block (a block is a "node" in our control flow).
A variable is locally exposed when it is referenced without being defined in the block.
Another important note is that a variable can be "overridden" (killed) in a block if it is locally exposed, and then made locally available.

$DF_i = \sum\limits_{j=1}^{\parallel V_i \parallel} DEF(v_j)$

Where $V_i$ is the set of exposed variables in this block and $DEF(v_j)$ counts the number of available definitions for $v_j$; that is, for j from 1 to length(the set of variables that reach block i) add the number of definitions of variable j to the running total.
The example given by Oviedo is:
\begin{quotation}
	For example, if block $n_i$ consists of the statements \textit{X = Y; A = B * Y * ( C - B ); Z = 2 + A;} and the set of variables definitions that can reach block $n_i$ is $R_i ~= ~{C_j, B_j, Y_k, B_k}$ (ie, if the definitions of C and B in block $n_j$ and the definitions of Y and B in block $n_k$ can reach block $n_j$), then, according to Definition 8, we have $V_i ~= ~{C,B,Y}$ and
	
	$DF_j = \sum\limits_{j=1}^{3} DEF(v_j)~=~4$ \cite{oviedo1993control}
\end{quotation}

Although some authors (for example, the author of the very well known Principles of Compiler Design \cite{aho1977principles}) utilize DEF alongside other functions, such as KILL and USE, these are not described in Oviedo's paper.

$DF = \sum\limits_{i=1}^{\parallel S \parallel} DF_i+DF_f$

Where S is all nodes except the start and the end nodes.

As with McCabes metric, we conclude this section with a re-iteration of some of the flaws found within this metric.
Once again we turn to the very insightful work done by Weyuker, who found Data-flow complexity to be lacking in most of the same areas as McCabes metric, but better most notably in her property 6.

\begin{itemize}
	\item Property 2: As with McCabes metric, there are not a finite number of programs for a given complexity.
	\item Property 5: Data Flow looks at blocks of data to derive complexity; if we combined two blocks in such a way that definitions of variables in the second block are now defined by the first block, the overal complexity would go
\end{itemize}

\subsection{Entropy}

The measure of a programs entropy as a form of complexity is based off of the work done by Shannon \cite{shannon2001mathematical}.
We allude to this during the discussion of Normalized Compression Distance in section \ref{icedutech}, and calculate the entropy of blog posts (as a stand in for code) in section \ref{generative_justice}.
Here, we estimated the entropy of programs by utilizing GZIP as a representation of Kolmogorov compression.
More about this can be found in the NCD discussion.
We also go into great detail about this metric and how Weyuker's properties apply in the same section.

Our implementation builds upon our toolchain which finds and analyzes each class; it simply calls a bash script and saves the raw and compressed file size to a CSV file.
Our actual analytics software later computes the difference.

\begin{lstlisting}[language=Bash]
#!/bin/bash

echo "filename,uncompress_size,compressed_size" > entropy.csv

for j in $(find issta/artifact/GooglePlayStore/ -name \*.apk); do
  ./analyze_class.sh $j |& tee log_$(basename $j).txt
  # Compress the file and compare it's size
  DEST=$(basename "$j")_archive
  unzip $j -d $DEST
  gzip -k -9 $DEST/classes.dex
  echo -n "$(basename "$j")," >> entropy.csv
  echo -n $(stat -c%s "$DEST/classes.dex"), >> entropy.csv
  echo $(stat -c%s "$DEST/classes.dex.gz") >> entropy.csv
done
\end{lstlisting}

\subsection{CK Metrics Suite}

Chidamber and Kemerer described a suite of metrics meant to applied to object oriented papers in their 1994 \cite{chidamber1994metrics} paper.
They describe a total of six metrics:
\begin{itemize}
	\item WMC - Weighted Methods per Class
	\item DIT - Depth of Inheritance Tree
	\item NOC - Number of Children
	\item CBO -Coupling Between Objects
	\item RFC - Response for a Class
	\item LCOM - Lack of Cohesion of Methods
\end{itemize}

Some of these are easier to conceptualize than others, and all are explained in detail in the 1994 paper.
Chidamber and Kemerer also use Weyuker's properties to analyze their metrics; most of their metrics failed one or more of the properties, but between them account for all.

Weighted Methods per Class is simply the sum of the complexity of the methods within the class.
However, CK do not go into detail discussing what this complexity is, but state that "If all method complexities are considered to be in unity, then WMC = n, the number of methods."
We used this simplified definition for this analysis.

Note that we skip classes which belong to libraries used by the application; this decision was made by the logic that supporting libraries should not be considered part of the original code base, and should be evaluated independently in a future experiment

Depth of Inheritance is exactly what it sounds like; for a given class, how for down the inheritance tree is it?

Note that because Java does not support multiple inheritance we do not need to account for finding the "longest" chain of parents.

Number of Children, also referred to as "fan-out", is the number of direct children a given class has.

Coupling Between Objects is the number of other classes that are either referenced by this class, or reference this class.

This was substantially more complicated to implement; and as we noted in the problems section, we had to call the methods for Cyclomatic Complexity and Data Flow complexity directly here, rather than integrating them into the pipeline used by Soot.
Another interesting point is that we had to be a bit hacky to prevent Soot from getting angry about us looking at more than one class at a time; hence the synchronize statement.

Response for Class is more interesting; it is the number of methods, both class specific and belonging to other classes, which will be executed in response to a message given to a class.
For our work we interpreted message to mean calling a publically exposed method on the class.

Lack of Cohesion of Methods is the count of pairs of methods within a class who do not access any overlapping class attributes.
This metric has come under much scrutiny, and so there have been multiple LCOM's made; the one described by CK is often referred to as LCOM1.
This particular metric provided a substantial obstacle with the tools we used for our analysis, as the tools don't provide an interface for finding references to the same class attributes.
As a result, this metric was not implemented.

\section{Excluded Metrics}

There are many metrics, and we did not include more than the ten listed above.
This was partially due to time constraints, and as an effect of that certain metrics which would prove difficult to implement due to technical constraints were skipped.
Some of the most notable are:

\begin{enumerate}
	\item Halstead's Measure -- this would be difficult to implement since Soot provides no functionality to account for the name of variables; perhaps due to the fact that such meta information is lost when the program is compiled, and since Soot inspects Java bytecode, it does not have access to this information
\end{enumerate}

\subsection{Software Description and Location}

The software and raw data used for this research can be obtained online at \url{https://tracker.logrit.com/repo/chathaway/soot_testing/}

To create our metrics, we not only extended several interfaces exposed by Soot, but also had to modify the framework to run a custom jimple pack.
This modification was not passed back upstream because it broke several architectural patterns of the framework; however, we provide an exact image of our modified framework at the source given above.

From the perspective of Soot, we created two types of metrics; metrics which iterate over all functions, and metrics which iterate over all classes.
These extend the BodyTransformer and SceneTransformer respectively; although neither actually transform the program, and could be used in conjuction.

Further along in development we encountered issues in getting both of these, the BodyTransformer and SceneTransformer, to run in the same session.
Our solution to this was to call the BodyTransformers from within the SceneTransformers, which is not a maintainable decision, but was time efficient for the scope of this project.

\section{Results}

The below charts show the median metric-values versus the number of reported leaks.
There does not appear to be a strong correlation for any of these values, perhaps due to the fact that an overwhelming number of reported values are very boring in nature; without the granularity of examining functions, we can only report on everything, including large number of classes which have no children and very simple method definitions.

\begin{figure}[H]

	\begin{minipage}{.5\textwidth}
		\centering
		\label{taint:df_vs_droid_infer}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_CC.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus cyclomatic complexity}
		
		\label{taint:df_vs_droid_infer}
		\centering
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_CK-DIT.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus CK depth of inheritance tree}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\label{taint:df_vs_droid_infer}
		\centering
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_CK-NOC.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus CK number of children}
		
		\label{taint:df_vs_droid_infer}
		\centering
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_CK-WMC.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus CK weighted methods per class}
	\end{minipage}%
\end{figure}


\begin{figure}[H]
	\begin{minipage}{.5\textwidth}
		\label{taint:df_vs_droid_infer}
		\centering
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_entropy.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus entropy}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\label{taint:df_vs_droid_infer}
		\centering
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/plot_DF.png}
		\end{adjustbox}
				\captionsetup{justification=centering,margin=1cm}\caption{Graph of reported leaks versus dataflow}
	\end{minipage}%
\end{figure}

%%% BAR CHARTS

These charts show the various metrics of all apps analyzed; at a visual inspection, there appears to be a very small link between CK-WMC and droid\_infer.
We provide a more numerical analysis below, but there appears to be a correlation in at least 50\% of the cases.

We have opted to show a sampling of charts in favor of all 70 or so.
If you wish to view all the data, please obtain it from the source repository given above.

\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-smashhit-com-mediocre-smashhit-1-3-3-10303-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK smashhit-com-mediocre...}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		
		
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-com-mobilityware-solitaire-3-0-3-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK com-mobilityware-solitaire...}
	\end{minipage}%
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-skype-com-skype-raider-5-1-0-57240-84008856-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK skype-com-skype-raider}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-twitter-com-twitter-android-5-32-0-3030745-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK twitter-com-twitter-android...}
	\end{minipage}%

\end{figure}


\begin{figure}[H]
	\centering

	\begin{minipage}{.5\textwidth}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-com-clearchannel-iheartradio-controller-4-11-0-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK com-clearchannel-iheartradio...}
		
		
		
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-com-withbuddies-dice-free-3-3-5-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK com-withbuddies-dice-free...}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		
		
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-dumbways2-air-au-com-metro-dumbwaystodie2-1-1-1-1001001-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK dumbways2-air-au-com-metro-dumbwaystodie2...}
		
		
		
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/bar-com-walmart-android-1-7-2-apk.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{All metric values for APK com-walmart-android...}
	\end{minipage}%
\end{figure}

In the below graphs, we show a variety of potential models that we attempt to fit to our data.
We skipped the models for CK-NOC and CC because the Tukey median of these values was the same across all data sets; hence, a very boring plot of a straight line of points.

Although most of these graphs show just about no correlation, several models found some correlation between reported taints and some complexity metric.
Most of these correlations are not strong, but may be worth reporting.
We have removed many graphs that show no correlation, leaving just one for each model, except for the case discussed below, to help reduce the page count of this thesis.
If you wish to see the others, please get the raw data from the repository specified above.

Figures \ref{fit:CK-CBO-BreitWignerModel} through \ref{fit:CK-CBO-VoigtModel} show a moderate correlation between the number of taints and the amount of coupling between object classes; the highest reported by the Moffatt model with an (absolute) correlation greater than 0.98 for C(sigma, amplitude), C(amplitude, center), C(sigma, center), C(beta, center), C(amplitude, beta), and C(sigma, beta).

Although we can see some small correlation in figures \ref{fit:entropy-BreitWignerModel}, and \ref{fit:CK-RFC-BreitWignerModel}, these correlations are not high enough to merrit further investigation

%% BEGIN FIT FUNCTIONS

\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
	
	\label{fit:CK-DIT-BreitWignerModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-DIT-droid-infer-BreitWignerModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-DIT vs droid\_infer \\ using Breit Wigner Model}
	
	
	
	\label{fit:CK-DIT-DampedOscillatorModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-DIT-droid-infer-DampedOscillatorModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-DIT vs droid\_infer \\ using Damped Oscillator Model}
	\label{fit:CK-WMC-DampedOscillatorModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-WMC-droid-infer-DampedOscillatorModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-WMC vs droid\_infer \\ using Damped Oscillator Model}
\end{minipage}%
\begin{minipage}{.5\textwidth}
	\label{fit:CK-DIT-VoigtModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-DIT-droid-infer-VoigtModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-DIT vs droid\_infer \\ using Voigt Model}
	
	
	
	\label{fit:CK-RFC-BreitWignerModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-RFC-droid-infer-BreitWignerModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-RFC vs droid\_infer \\ using Breit Wigner Model}
	\label{fit:CK-WMC-BreitWignerModel}
	\begin{adjustbox}{max width=\columnwidth}
		\includegraphics{./taint_analysis/fit-CK-WMC-droid-infer-BreitWignerModel.png}
	\end{adjustbox}
		\captionsetup{justification=centering}\caption{CK-WMC vs droid\_infer \\ using Breit Wigner Model}
\end{minipage}%
\end{figure}

\begin{figure}[H]
\centering
	\begin{minipage}{.5\textwidth}
		
		
		\label{fit:CK-CBO-BreitWignerModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-BreitWignerModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Breit Wigner Model}
		
		
		\label{fit:CK-CBO-GaussianModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-GaussianModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Gaussian Model}
		\label{fit:CK-CBO-LognormalModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-LognormalModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Lognormal Model}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\label{fit:DF-BreitWignerModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-DF-droid-infer-BreitWignerModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{DF vs droid\_infer \\ using Breit Wigner Model}
		
		
		
		\label{fit:DF-DampedOscillatorModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-DF-droid-infer-DampedOscillatorModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{DF vs droid\_infer \\ using Damped Oscillator Model}
		\label{fit:CK-CBO-LinearModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-LinearModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Linear Model}
	\end{minipage}%
\end{figure}


\begin{figure}[H]
\centering

	\begin{minipage}{.5\textwidth}
		\label{fit:CK-CBO-LorentzianModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-LorentzianModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Lorentzian Model}
		\label{fit:CK-CBO-ParabolicModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-ParabolicModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Parabolic Model}
		\label{fit:CK-CBO-PowerLawModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-PowerLawModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Power Law Model}
		
		
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\label{fit:CK-CBO-MoffatModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-MoffatModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Moffat Model}
		\label{fit:CK-CBO-Pearson7Model}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-Pearson7Model.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Pearson7Model}
		\label{fit:CK-CBO-PseudoVoigtModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-PseudoVoigtModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Pseudo Voigt Model}
	\end{minipage}%
\end{figure}


\begin{figure}[H]
\centering
	
	\begin{minipage}{.5\textwidth}
		\label{fit:CK-CBO-QuadraticModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-QuadraticModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Quadratic Model}
		\label{fit:CK-CBO-VoigtModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-VoigtModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Voigt Model}
	\end{minipage}%
	
	\begin{minipage}{.5\textwidth}
		\label{fit:CK-CBO-SkewedGaussianModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-CK-CBO-droid-infer-SkewedGaussianModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{CK-CBO vs droid\_infer, using Skewed Gaussian Model}
	\end{minipage}%
\end{figure}


\begin{figure}[H]
	\centering

	\begin{minipage}{.5\textwidth}
		\label{fit:entropy-DampedOscillatorModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-entropy-droid-infer-DampedOscillatorModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{entropy vs droid\_infer, using Damped Oscillator Model}
		\label{fit:entropy-BreitWignerModel}
		\begin{adjustbox}{max width=\columnwidth}
			\includegraphics{./taint_analysis/fit-entropy-droid-infer-BreitWignerModel.png}
		\end{adjustbox}
				\captionsetup{justification=centering}\caption{entropy vs droid\_infer, using Breit Wigner Model}
	\end{minipage}%
\end{figure}

\section{Conclusion}

Despite our hope that there would be some substantial correlation between complexity and the likelyhood of a program containing leaks, we are sad to report that even our strongest correlation is questionable, at best.
Combined with the cost of computing a number of these metrics (some of which involve a significant amount of time due to the way their programs were coded/obfuscated), we do not believe there is a clear connection benefit between the two.

It should be noted that had we the resources to implement a taint-measuring program which counted not only the number of leaks, but also returned the name and number of functions and classes that contained the leaks, we may have gotten much more substantial results.
This is perhaps an area that can be explored at a later time when the expertise necessary to comprehend and modify the DroidFlow program is more available.

This is a good example of a limitation of software complexity metrics; they should not be used in place of static semantic analysis tools.
However, it is still possible that in other application, these metrics could compliment those tools.
If one imagines the semantic analysis as a more absolute function, something that will very carefully examine every line of code, similar to the grammar and syntax trees often used in natural language, complexity metrics could be imagined as more bottom-up approach.
Rather than reading every line with certainty, they examine the general trend of the behavior of software; more like a statistical approach.
