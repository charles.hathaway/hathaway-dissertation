% vim: spell spelllang=en_us
\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[style=ieee,backend=bibtex]{biblatex}
\bibliography{biblo}

\title{Interactive Complexity: Measures for Software Systems}
\author{Charles Hathaway}

\begin{document}

\maketitle

\begin{abstract}
With even the most trivial of applications now being written on top of millions of lines code of libraries, API's, and programming languages, much of the complexity that used to exist when designing software has been abstracted away to allow programmers to focus on primarily business logic.
With each application relying so heavily on the ecosystem it was designed to run in, whether that is limited to a local system or includes dependencies on machines connected by networks, measuring the complexity of these systems can no longer be done simply by observing the code internal to the application; we also need to account for its external interactions.
This is especially important when considering issues such as security, stability, and general correctness, which becomes more vital as our healthcare, financial, and automobiles rely on complicated software systems.
We propose Interactive Complexity, which attempts to provide a quantitative measure of how intertwined parts of the system are.

Some of the most well-known software complexity metrics out there are the metrics in the CK-metric suite\cite{chidamber1994metrics}; these metrics are designed for use in measuring object oriented systems, but we believe they can be adapted to help measure the interaction of software systems.

\end{abstract}

\section{Introduction}

Despite many years of development and contemplation, only a handful of software metrics have succeeded in becoming more than just a glint in the eye of their creators.
Of the few metrics that made it to fruition, the most successful has been, without a doubt, the CK-metric suite proposed to measure the quality of object oriented systems.
Rather than just being formulated with the general intent of measuring the complexity of a piece of software, they were designed to help predict the cost of maintaining the software over a period of time; in essence, this is a more practical definition of software quality.
In this paper, we aim to take these tried and tested metrics and extend them to a broader domain; instead of looking at a piece of software as a standalone entity, whether it be composed of classes and objects or anonymous functions and monads, we would like to look at the ecosystem upon which it is built.

Why do we care about the ecosystem of the software product?
% Mention open versus closed systems. What do you mean by puzzle piece?
Software systems are all about building blocks, pieces of a complex puzzle that all fit together to achieve a function.
This is not dissimilar to the biological ecosystems that are talked about when discussing entropy and complexity; although biologists sometimes talk about the complexity of an organism, it is always in the context of the environment the organism exists in.
Often, there is a strong connection between how an organism interacts with its environment and the perceived complexity of the organism.
We believe that software, at least software being developed in this post-connected era, will behave much the same as an organism in its environment; it will thrive, or fail, not only based on what it excels at, but be strongly influenced by the success of the ecosystem it is based upon.

For example, Java Applets were a huge part of the early Internets success; whether it be small games, VPN's, or productivity applications, Java applets were a solid basis for making the application due to its ease of deployment and versatility.
And yet, if we go look at the internet today, these applets have all been replaced by HTML5/Javascript applications; why?
To get the answer, all you need to do is find a Java applet, and run it.
You will first have to get past several warning messages about how dangerous this is for your system, about how there are known vulnerabilities, and how your world will end should you run this Java applet.
After that, you will need to overcome the clunkiness caused by a lack of UI design work since the mid 2000s, when this type of application was its prime.

This example is very apparent to an end user, but how does this relate to those behind the code?
There are really two ways of writing code:
\begin{enumerate}
  \item Reinvent the wheel. Write all your code from scratch, and see if you can make it run without errors (or spend days tracking down why your mergesort isn't working).
  \item Use a library, and write a small program that does only what you need it to do.
\end{enumerate}

If you write the code from scratch, you can rest assured that it will continue to work as you wrote for a good long while (at least until the underlying system architecture changes).
However, if you write your code on top of a library, the moment the library gets and update, your code may suddenly cease to function.
What if the library was faulty to begin with?

We would like to provide measures that help answer these questions.
Not questions about the quality of a single piece of software, but questions about the quality of a system of software.

\section{Adapting CK-Metrics to Software Systems}

The CK-metric suite consists of 6 metrics geared towards object-oriented programming systems.
We would like to borrow from the work done by Chidamber and Kemerer, and apply their logic to the problem of software systems rather than an individual piece of software.
In this section, we will examine the original intent and definition of each CK-metric, then discuss our new definition and the reasoning behind each non-obvious decision.

Some of their metrics already explored the use of libraries, if not intentionally.
Namely, the DIT (depth of inheritance tree) metric, which would examine the list of classes that an application class was built on top of.
Aside from this metric, none of the others explicitly looked at classes declared in a library; if they happened upon one while calculating the value for an application class, it may perform some metric calculations to help resolve the application class metrics, but it wouldn't continue to explore the library.
For the purposes of the IC-metric suite, all of these metrics will be re-purposed; even if they had this tangential examination of related libraries.

\subsection{Defining libraries and dependencies}

Rather than briefly mention this question in the introduction, we will delve into what we consider a library, since there are some nuances that need to be considered.
The most obvious definition would be something with the word "library" in the description, such as a library to parse HTML or a library to interface with an API.
These types of libraries are usually designed as part of the development of a particular project; they are not often made for the sole purpose of just because.
As such, they don't directly interface with the end-user (this is usually the domain of another module in the project).

One of the first problems to arise with that definition is that it doesn't account for facilities built into the programming language or operating system itself.
For example, is a call to the C function \texttt{sizeof} considered a library call?
What about a call to \texttt{Enum.reduce} function in Elixir?
Then there are the system calls we see all over the place, such as \texttt{System.out.println} in Java?

There is also the case of a call to an external system; suppose one wants to get a list of stock prices, and the stock broker has a system failure.
That failure will be reflected in the calling application, even though we didn't directly include the API in our list of libraries.
We would have a record of including a REST client, but we have no way of knowing if that client is used to access the stock broker API, or that it is used to access more than one API.

These types of questions are what led us away from simply making a metric which looked at the metadata surrounding a project (such as its \texttt{<dependencies>} tag in Maven), and towards a metric suite which examined more than one definition of library.
For us, a library is any external dependency; whether it be operating system code, programming language features, third-party libraries, external systems, or modules loaded at run time.
Our goal is to have something that can account for each of these dependencies in some way.

\subsection{Weighted Methods per Class}

Chidamber and Kemerer originally thought of this metric as the number of methods per class times their complexity; however, since there was (and still is not) a good metric for determining complexity, that value was just left as 1 for all methods.
Therefore, this value became the number of methods per class.

In our iteration of these metrics, it helps to conceptualize a program as a class; with that scaffolding, this still doesn't have a clear mapping.
One could imagine it being the number of classes in the program, but since we aren't truly interested in how this program interacts with itself, this isn't of much interest to us.
Instead, if you consider a program to be a function of the libraries it depends on, we can see how this would become simply the count of the number of dependencies.

\subsection{Depth of Inheritance Tree}

As discussed, this was meant to be the number of parents until a class reaches the base object; the last item with no parents.
We can easily map this to the depth of the dependency tree; keep looking at parents until we find a library that doesn't have a parent, and take the length of the longest chain.

\subsection{Number of Children}

Although easy to envision, the implementation details of this metric are problematic.
We could use the number of projects that rely on this one as the number of children, but since the original articulation of the metric required a closed system, and we are now in an open system, how can we measure this?

We can get a reasonable approximation by looking at the meta-data available through the Maven repository.
One option would be to scan all projects in Maven for projects that list this as a dependency; this would be very tedious and consume many resources.
Instead, we can imagine that the number of downloads would be a function of the number of children by the popularity of those children.

The trouble with using downloads is that we may get bloated numbers for very popular projects; if everything relies on this project, it will be downloaded for not only every child, but every child's children.
Rather than attempting to compensate for this, our first case study will do the following:
\begin{enumerate}
  \item Use the raw value, $NOC_{raw}$
  \item Use the raw value scaled down, $\sqrt{NOC_{raw}}$
\end{enumerate}

Our hope is that one of these values will give strong correlations; if not, we will have to explore the more tedious route.

\subsection{Coupling between Object Classes}

The coupling between object classes makes a lot of sense when you're talking about classes; they should be aware of each other, and know how to call methods on each others objects.
When we're talking about libraries, this is less desirable.
A well written library should not depend on the implementation of it's children to dictate its functionality; therefore, we want this value to be as close to 0 as possible.
There are a few cases (paired libraries) where we might be able to envision some cyclic dependencies, but that will most likely be an exception rather than a rule.

\subsection{Response for a Class}

This value was meant to be a count of the possible function calls that could result from a function on the class being called; that is, explore all code paths in each public function and count the number of function calls.
We can easily translate this to talk about systems if we accept just a first-step measure of function calls.
This value then becomes the number of unique methods called in libraries that the application depends on.

\subsection{Lack of Cohesion of Methods}

CK original meant this to be the number of unused methods, if you will.
The exact meaning has come under fire in the past, and several replacements have been proposed.

For the IC-metric suite, we propose that this value represent the number of functions not used in an exposed libraries API.
The logic for this is simple; if I include numpy just to multiply two arrays, I'm not making use of all the features it provides, and therefore it should not be as heavily weighted as my including of another library which my entire application is based upon.
This is slightly account for in the RFC metric, but should be more emphasized.

\section{Case Study}

Our case study utilized the Software Metrics Framework to analyzze a large number of projects over a series of time, measured in releases.
We analyzed 150 projects, randomly selected from the Apache foundations selection, so long as they matched the following criteria:

\begin{itemize}
  \item They must have a Git mirror
  \item They must use JIRA for issue tracking
  \item They must be Java projects that use Maven
\end{itemize}

SMF determines these attributes for us by checking the Apache JIRA installation for the presence of the project, and the source repository for the presence of a pom.xml file.

Figure N shows a complete list of selected projects.

\begin{itemize}
\item HTTPCLIENT
\item SPR
\item language\_tool
\item LOG4J2
\item OOZIE
\item WINK
\item ALLURE
\item BOOKKEPPER
\item ABDERA
\item ACCUMULO-BSP
\item ACCUMULO-EXAMPLES
\item ACCUMULO-INSTAMO-ARCHETYPE
\item ACCUMULO-PIG
\item ACCUMULO-TESTING
\item ACCUMULO-WIKISEARCH
\item ACCUMULO
\item ACTIVEMQ-ACTIVEIO
\item ACTIVEMQ-APOLLO
\item ACTIVEMQ-ARTEMIS
\item ACTIVEMQ-CLI-TOOLS
\item ACTIVEMQ-NMS-OPENWIRE-GENERATOR
\item ACTIVEMQ-OPENWIRE
\item ACTIVEMQ-PROTOBUF
\item AIRAVATA
\item AMBARI
\item ANY23
\item APEX-CORE
\item APEX-MALHAR
\item ARCHIVA-REDBACK-CORE
\item ARCHIVA-SITE
\item ARCHIVA
\item ARIES-CONTAINERS
\item ARIES-JAX-RS-WHITEBOARD
\item ARIES-JPA
\item ARIES-RSA
\item ARIES
\item ASTERIXDB-BAD
\item ASTERIXDB
\item AVRO
\item AXIS1-JAVA
\item AXIS2-JAVA
\item AXIS2-TRANSPORTS
\item BAHIR-FLINK
\item BAHIR
\item BATIK
\item BEAM
\item BIGTOP
\item BROOKLYN-CLIENT
\item BROOKLYN-DIST
\item BROOKLYN-LIBRARY
\item BROOKLYN-SERVER
\item BROOKLYN-UI
\item BROOKLYN
\item BVAL
\item CALCITE-AVATICA
\item CALCITE
\item CAMEL
\item CARBONDATA-SITE
\item CARBONDATA
\item CAYENNE-MODELER
\item CAYENNE
\item CHAINSAW
\item CHEMISTRY-OPENCMIS
\item CHUKWA
\item CLEREZZA-RDF-CORE
\item CLEREZZA
\item COCOON
\item COMMONS-BCEL
\item COMMONS-BEANUTILS
\item COMMONS-BSF
\item COMMONS-BUILD-PLUGIN
\item COMMONS-CHAIN
\item COMMONS-CLI
\item COMMONS-CODEC
\item COMMONS-COLLECTIONS
\item COMMONS-COMPRESS
\item COMMONS-CONFIGURATION
\item COMMONS-CRYPTO
\item COMMONS-CSV
\item COMMONS-DAEMON
\item COMMONS-DBCP
\item COMMONS-DBUTILS
\item COMMONS-DIGESTER
\item COMMONS-DISCOVERY
\item COMMONS-EMAIL
\item COMMONS-EXEC
\item COMMONS-FILEUPLOAD
\item COMMONS-FUNCTOR
\item COMMONS-IMAGING
\item COMMONS-IO
\item COMMONS-JCI
\item COMMONS-JCS
\item COMMONS-JELLY
\item COMMONS-JEXL
\item COMMONS-JXPATH
\item COMMONS-LANG
\item COMMONS-LAUNCHER
\item COMMONS-LOGGING
\item COMMONS-MATH
\item COMMONS-NET
\item COMMONS-NUMBERS
\item COMMONS-OGNL
\item COMMONS-POOL
\item COMMONS-PROXY
\item COMMONS-RDF
\item COMMONS-RNG
\item COMMONS-SCXML
\item COMMONS-TEXT
\item COMMONS-TRANSACTION
\item COMMONS-VALIDATOR
\item COMMONS-VFS
\item COMMONS-WEAVER
\item CONTINUUM
\item CREADUR-RAT
\item CREADUR-TENTACLES
\item CREADUR-WHISKER
\item CRUNCH
\item CTAKES
\item CURATOR
\item CXF-BUILD-UTILS
\item CXF-DOSGI
\item CXF-FEDIZ
\item CXF-XJC-UTILS
\item CXF
\item DDLUTILS
\item DELTASPIKE
\item DIRECTMEMORY-LIGHTNING
\item DIRECTMEMORY
\item DIRECTORY-DAEMON
\item DIRECTORY-FORTRESS-COMMANDER
\item DIRECTORY-FORTRESS-CORE
\item DIRECTORY-FORTRESS-ENMASSE
\item DIRECTORY-FORTRESS-REALM
\item DIRECTORY-INSTALLERS
\item DIRECTORY-KERBY
\item DIRECTORY-PROJECT
\item DIRECTORY-SERVER
\item DIRECTORY-SHARED
\item DIRECTORY-SKINS
\item DIRECTORY-STUDIO-PLUGIN
\item INCUBATOR-MRQL
\item HCATALOG
\item MAVEN-INDEXER
\item ISIS
\item MAVEN-DOXIA
\item JSPWIKI
\item PARQUET-MR
\item INCUBATOR-OMID
\item TILES-REQUEST
\item MAVEN-SKINS
\item RAVE
\item SHINDIG
\item TAPESTRY3
\item SERVICEMIX3
\item INCUBATOR-SLIDER
\item GERONIMO-TXMANAGER
\end{itemize}

\subsection{Problems Encountered}

During this case study, a number of issues were encountered.
Some of them required changes to the initial planned scope of the case study.

\begin{enumerate}
  \item Docker VM was slow
    \begin{itemize}
      \item Moved out of Docker, and run on bare metal
    \end{itemize}
  \item Exponentional growth of parent tree
    \begin{itemize}
      \item Use techniques from web graphs; limit number of explored parents
      \item Limiting number of explored parents doesn't lower quality due to the "6 degrees of seperation" theory
    \end{itemize}
  \item Slow pom.xml scanning
    \begin{itemize}
      \item Wrote custom Maven plugin to perform multiple scans after one pass of pom.xml
    \end{itemize}
  \item Rate limiting of Maven repositories
    \begin{itemize}
      \item Add handling of SIGINT to delay project execution
    \end{itemize}
\end{enumerate}

\section{Conclusion}

\printbibliography

\end{document}
