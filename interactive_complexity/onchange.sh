#!/bin/bash

pdflatex -halt-on-error main.tex
bibtex main
pdflatex -halt-on-error main.tex

# detex index.tex | languagetool -l en-US -
