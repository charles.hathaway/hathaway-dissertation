\section{Introduction}

Despite many years of development and contemplation, only a handful of software metrics have succeeded in becoming more than just a glint in the eye of their creators \cite{fenton2014software}.
Of the few metrics that made it to fruition, the most successful have been the CK-metric suite, created for measuring the complexity of   object oriented systems \cite{tang_empirical_1999} \cite{subramanyam2003empirical}.
Rather than just being formulated as an abstract theory of  software complexity, they were designed specifically to help predict the cost of maintaining the software over a period of time; in essence, a practical metric for one aspect of software quality.
In this chapter, we aim to take this popular set of  metrics and extend them to a broader domain: instead of looking at an application as a standalone entity, we would like to look at its relations to ecosystem upon which it is built.

Previous works that examined software complexity have focused on a variety of goals.
 McCabe was looking for a metric to quantify the number of testing paths within the code, his belief being that reducing the number of code paths that needed to be tested would increase test coverage.
Others \cite{munson1991use} have been concerned with software reliability.
Another common usage has been in quantifying the cost of maintaining the software over a period of time; typically by an indicator of the probability of faults that a software project would have.
In this chapter, we explore the latter, as it has been more widely adopted by the previous literature and is easier to measure.

It is increasingly common in literature on networks to compare software systems to biological systems \cite{mens2014studying}.
This analogy is very helpful in thinking about a metric  for interactive complexity.
Just as ecologists draw a portrait of an organism's properties by looking at its relations to the surrounding ecosystem , we need to look at a software project's relation to the digital networks in which it is embedded into get a better understanding of its complexity.
Within a biological system, one needs to examine how an organism interacts with flows of biotic substances and resources:  does it provide a food source? Make homes for others, like beavers or coral? Convert nitrogen in the air to nitrogen in the soil? Pass along viruses? Does it consume a great deal of other resources within the system?

One way ecologists summarize these distinctions is the contrast between "keystone species"--organisms that have so many crucial outgoing links that the ecosystem would be drastically affected if they vanished--and "umbrella species" that take in a great variety of resources but do not have much effect if they vanish \cite{mills1993keystone} \cite{roberge2004usefulness}.
The honeybee is a good example of a keystone species: it is internally simple, but it pollinates hundreds of different plants, upon whom thousands of other species depend.
At the other end of the spectrum, we have umbrella species such as the Siberian tiger: internally far more complex than a bee, but its ecosystem relationships are primarily incoming links, spread over a very large range (it would make little change to the ecosystem if it vanishes).


We believe that software developed in this hyper-connected era also exists on such a spectrum of interactivity between keystone and umbrella species.
Some software systems are simple, but have huge impacts on the environment (for example, JUnit), and other systems depend on huge numbers of libraries to function, but are not themselves dependencies for anything else (for example, Apache Ambari).
These are the bees and tigers, respectively.

Understanding where in the spectrum between keystone species and umbrella species a particular software project might lie provides some useful insights.
For example, Java Applets were a huge part of the early Internet's success;  a solid basis for making in-browser applications due to its ease of deployment and versatility. While the many of these were in the realm of "creative coding"--tiny games, mini-simulations and so on--the sheer numbers made it one of the most successful umbrella species of the coding world. 
And yet, if we go look at the Internet today, these applets have all been replaced. Explanations for the disappearance vary: a 2014 Cisco report claimed that over 90\% of hacker attacks were directed towards Java, implying that it was strictly a security concern. Oracle, on the other hand, implied that browsers for mobile devices were eliminating java plugins to save space, and the same browsers were then eliminating plugins for desktops to maintain consistency, therefore necessitating its elimination. Either way,
this is a case of a keystone species becoming endangered, and resulting in the demise of all umbrella species which relied on it.

Interactive complexity could have many different applications.
For example, if you write code from scratch, you can rest assured that it will continue to work as you wrote for a good long while (at least until the underlying system architecture changes). However you may be reinventing the wheel, needlessly recreating functions that already exist in a library and adding enormous numbers of lines of code to your system.
On the other hand, if you write your code on top of a library, the moment the library gets an update, your code may suddenly cease to function.
We hope that Interactive Complexity might help us to find the sweet spot; the place where using libraries reduces the initial complexity of software, but doesn't cause us to enter "dependency hell".
Other applications might be in security (fewer dependencies means fewer vectors for viral transmission); reliability (reliance on untested libraries); project management (too much in-house code might indicate that external libraries are under-utilized); education (a student who does not use available libraries might be missing techniques of skills); and so on.
It also has possible applications to basic research: do certain classes of projects tend towards greater interactive complexity over time?

So far we have discussed interactive complexity as a general concept. For the purposes of this article we will limit ourselves to defining its  metrics using adaptations of  the CK-metric suite, but we should note that is only one possible set of metrics for interactive complexity. Follow a description of how we adapted the CK suite, we then discuss our case study, done with the Software Metrics Framework \cite{software_metrics_framework}, which provides tools help rerun this study on new metrics and ideas.
Lastly, we examine the results of our case study, including examining some key projects which we believe represent a good sample of the things we indexed.

It is worth noting that examining how software communities are connected has been of great interest in recent years.
Batista et al have recently published work examining the way developer communities on Github interact, and how these interactions build teams \cite{batista_collaboration_2017}.
In many ways, the work we present here is related, with the key distinction being that we are looking at the software dependency network instead of the developer network.

\section{Adapting CK-Metrics to Software Systems}

In this section, we will examine the original intent and definition of each of the 6 metrics created by Chidamber and Kemerer \cite{chidamber1994metrics}, then discuss our new definition, applying their logic to the problem of software systems rather than an individual piece of software.



One of their metrics already explored the use of libraries, albeit not with the intention of distinguishing internal and external dependencies. Their DIT (depth of inheritance tree) metric examines the list of classes that an application class was built on top of.
Aside from this metric, none of the others explicitly looked at classes declared in a library; if they happened upon one while calculating the value for an application class, it may perform some metric calculations to help resolve the application class metrics, but it wouldn't continue to explore the library.
For the IC-metric suite, all of these metrics will be re-purposed; even if they had this tangential examination of related libraries.

\subsection{Defining external dependencies}


 What exactly constitutes an external dependency?  
It is possible to use interactive complexity in a broader sense as any information exchanges between the application and its environment, but here we will restrict our metrics to only those dependencies created prior to execution.  
For the purposes of this paper, an external dependency is any third-party library, external system, API or similar external module loaded prior to run time. One of the easiest ways to do that is look at the metadata surrounding a project (such as its \texttt{<dependencies>} tag in Maven) or examining bytecode to find symbols (as in recent work done by \cite{lutellier2017measuring}). However that is not exhaustive; for example, a library can be used to access many external REST (REpresentational State Transfer) endpoints, each of which would be an external dependency. 
Our goal is to have something that can account for each of these dependencies in some way, and examine how these dependencies fit into the rest of the system.
To that end, we defined the following 6 adaptations of the CK metrics.

\subsection{Weighted Methods per Class (WMC)}

Chidamber and Kemerer originally thought of this metric as the number of methods per class times their complexity; however, since there has never been a good metric for determining complexity of a method, this is usually calculated simply as the count of methods per class.

% This needs reworking
In our iteration of these metrics, it helps to conceptualize a program as a class; with that scaffolding, we at least have a good avenue to ask questions.
One could imagine it being the number of classes in the program, but since we aren't truly interested in how this program interacts with itself, this isn't of much interest to us.
Instead, if we imagine a software system as a collection of components, where each component is an included library, we could envision mapping this to something like the sum of methods in the included libraries.

Our concern with doing the sum of methods in included libraries is related to the six degrees of separation concept; most projects will include one or two key libraries.
These libraries will likely include a lot of functionality, and so will artificially bloat the WMC score of projects.
With this in mind, we opted to take the number of dependant libraries as a metric, which is both an obvious and simple metric.
This is measured primarily using the metadata available in Maven, since we do not wish to count libraries multiple times if they are included in many files.


\subsection{Depth of Inheritance Tree (DIT)}

As discussed, this was meant to be the number of parents until a class reaches the base object; the last item with no parents.
We can easily map this to the depth of the dependency tree; keep looking at parents until we find a library that doesn't have a parent, and take the length of the longest chain.

Originally, we had thought of a dependency as any project and versions that was required by something; however, after some investigation we found that including the version as a criteria for selecting the parent resulted in exponential growth of the number of parents we needed to explore.
We briefly considered using concepts from 6-degrees of separation to help limit this growth, but after we stopped using versions as a qualifier, the growth was much more acceptable.

In the end, we ended up using the dependency plugin in Maven to construct a dependency tree, then took the maximum depth of that tree to be the DIT measure.
This was much simplier than initial efforts, which included dumping the list of dependencies across project (which often included multiple libraries) and then recursing on each one, until we found a project with no dependencies.

\subsection{Number of Children (NOC)}

%% CHH: update this section after pulling in mvnrepository data

Originally defined as the number of children that a particular class has (the number of classes which extend it), this metric very clearly maps to the number of projects that utilize the one we are examining.
It turns that in a closed system, like an application which you calculate CK metrics for, this is easy to calculate.
For an open system, like a project hosted by Apache, this is much harder.

We toyed with a number of methods for arriving at this metric, including:
\begin{enumerate}
  \item The number of downloads
  \item The number of references
\end{enumerate}

Our original idea of using the number of downloads as an indication of popularity was reasonable, but the data was hard to come by.
In addition, it didn't accurately reflect this metric as projects which didn't depend on a particular project might still download that project if another included it.
This would result in certain projects getting highly bloated numbers; for example, HTTPClient, which is a component of the HTTPComponents library, is only included by a few projects.
If we look at the number of downloads of this artifact, however, we see a massive increase in the number of downloads as HTTPComponents gets downloaded quite frequently as dependency of the Play framework.

So that leads us to trying to count the number of references.
One could imagine doing this by looking at the meta-data available through the Maven repository.
One option would be to scan all projects in Maven for projects that list this as a dependency; we can not reasonable do this with the resources available to us.
It would require mirroring many Maven repositories, trying to scan through them, and aggregating that data.

Thankfully, someone had already done this.
They published their data to the web, so we were able to download it and use it for our experiment.
We would like to extend immense gratitude to Dr. Fernando Rodriguez Olivera, who created and mainted the web resource https://mvnrepository.com/ which allowed us to examine this metric with very solid data \cite{olivera2017mvnrepository}.

\subsection{Coupling between Object Classes}

The coupling between object classes makes a lot of sense when you're talking about classes; they should be aware of each other, and know how to call methods on each others objects.
When we're talking about libraries, this is less desirable.
A well written library should not depend on the implementation of it's children to dictate its functionality; therefore, we want this value to be as close to 0 as possible.
There are a few cases (paired libraries) where we might be able to envision some cyclic dependencies, but that will most likely be an exception rather than a rule.

During our initial investigation into the DIT metric, we came across a number of cyclic dependencies.
After switching to a more simple approach, described above, the number of cycles greatly reduced; this suggests that there may be an interesting pattern to how projects add and remove dependencies.
This investigation was not done while writing this paper, as it has a series of challenging and interesting problems which would need to be addressed, including:
\begin{enumerate}
	\item Scaling; during some of our runs, we ended up with 30000 projects to explore. Creating a graph of this size would require some work
	\item Caching; to do this in a reasonable timeframe, we would benefit from a local mirror of the repositories. Without this, it was taking days to calculate the metric from a single project
\end{enumerate}

\subsection{Response for a Class}

This value was meant to be a count of the possible function calls that could result from a function on the class being called; that is, explore all code paths in each public function and count the number of function calls.
We can easily translate this to talk about systems if we accept just a first-step measure of function calls.
This value then becomes the number of unique methods calls exposed to applications that depend on this one.

We ended up using the Java disassembler to get the list of public functions for a class, and aggregate a list of the number of functions that would be called for those public functions.

\subsection{Lack of Cohesion of Methods}

Chidambar and Kemerer original meant this to be the count of the set of methods not used by a pair of classesl.
The exact meaning has come under fire in the past, and several replacements have been proposed.
For the Interactive Complexity (IC)-metric suite, we propose that this value represent the number of unused imports within a project.
The logic for this is simple; if I include numpy just to multiply two arrays, I'm not making use of all the features it provides, and therefore it should not be as heavily weighted as my including of another library which my entire application is based upon.

Initially, we had calculated this metric using the Soot framework; however, this was found to be rather slow.
Soot was intended for a different type of project, and was overkill for the analysis we were doing here; instead, we opted to use the Maven dependency plugin again, which had a feature to find the unused dependencies within a project.

\section{Case Study}

Our case study utilized the Software Metrics Framework \cite{software_metrics_framework} to analyze a large number of projects.
There are two possible approaches: in the synchronic approach, one compares projects of similar size, or uses some normalization factor to make them comparable.
This normalization is required to account for the fact that very large projects, regardless of interactive complexity, will have larger number of bugs than small projects.
In the diachronic approach, we compare the metric in one project as it changes over time, to the number of bugs as they change over time.
Essentially that is normalizing the project to itself.
We decided to use this diachronic approach for several reasons.
First, it solves the problem of finding the normalization factor (lines of code? Some other complexity metric?).
Second, the synchronic approach requires that one make a decision about which version of the software is used: the first version? The latest version? Third, by sampling the code at every marked release, we have many data points, so that even if this is far from the final version, we can determine a general trend.
Each release is a version of the code that was deemed of sufficient quality to be given to the public, and will have a list of faults there fixed in that version.

We analyzed 80 projects, which had a cumulative 1600 versions, 1000 of which were tagged in the version control system and would compile.
These projects were randomly selected from the Apache foundation's selection, so long as they matched the following criteria:

\begin{itemize}
  \item They must have a Git mirror
  \item They must use JIRA for issue tracking
  \item They must be Java projects that use Maven
\end{itemize}

SMF determines these attributes for us by checking the Apache JIRA installation for the projects presence, and the source repository for a pom.xml file.

As a test of our assumptions above, we examine the number of lines of code (LOC) as a metric.
There was indeed a very strong correlation to fault rate, suggesting that lack of normalization would lose the interactive complexity "signal" in the noise of project size effects.
LOC has been previously dismissed a poor measure of software complexity \cite{rosenberg_misconceptions_1997}.
It has been speculated the LOC is a good measure of the scale of a project; and for that reason, the larger the project, the more bugs it will have \cite{rosenberg_misconceptions_1997}.
Using LOC instead of our time interval as a synchronic normalizer would be an interesting experiment, and a possible avenue of further research.

Figure \ref{project_list} shows a complete list of selected projects, along with the median values for their metrics across all versions.
We believe this to be a reasonable sample size for statistical analysis, but believe that it is worth selecting a few representatives projects to provide concrete examples will add value to the discussion.
The criteria for these projects are:
\begin{itemize}
	\item They must have at least 10 versions which we could identify in the codebase
    \item 80\% of the versions analyzed must have successfully compiled; without this, only a subset of our metrics could be applied
    \item They must have non-zero counts of reported and fixed bugs in each version; without this, we would have nothing meaningful to analyze.
\end{itemize}

The projects that were selected as representative are the Apache Accumulo, Apache HTTPComponents Client library, Apache Rave, Apache CXF, and log4j2.
Figures \ref{accumulo}, \ref{httpclient}, \ref{rave}, \ref{cxf}, and \ref{log4j2} show the calculated metrics for these projects, alongside the bug fix count, for each versions that we were able to match to a commit in their VCS.

\subsection{Problems Encountered}

During this case study, a number of issues were encountered.
Some of them required changes to the initial planned scope of the case study.

\begin{enumerate}
  \item Docker VM was slow; initial hope was to ease the use of SMF by using Docker
    \begin{itemize}
      \item Moved out of Docker, and run on bare metal
    \end{itemize}
  \item Exponentional growth of parent tree when exploring DIT
    \begin{itemize}
      \item Use techniques from web graphs; limit number of explored parents
      \item Limiting number of explored parents doesn't lower quality due to the "6 degrees of seperation" theory
    \end{itemize}
  \item Slow pom.xml scanning
    \begin{itemize}
      \item Wrote custom Maven plugin to perform multiple scans after one pass of pom.xml
    \end{itemize}
  \item Rate limiting of Maven repositories
    \begin{itemize}
      \item Add handling of SIGINT to delay project execution
    \end{itemize}
\end{enumerate}

\section{Results and Interpretation}

For reference, the metrics we are examining are:
\begin{enumerate}
  \item IC-NOC: Number of Children; number of projects that depend on this one
  \item IC-DIT: Depth of the Inheritance Tree; longest chain of parent dependencies
  \item IC-LCOM1: Lack of Cohesion Methods; imports unused within the project, but included anyway
  \item IC-WMC: Weighted methods per class; number of libraries that this one depends on
  \item IC-RFC: Response For Class; number of publically exposed methods
\end{enumerate}

Figure \ref{combined_pearsons} shows the correlation and p-value of each metric across all projects; of great note is the low p-values of IC-NOC, IC-RFC and IC-LCOM1.

Figure \ref{p_values} shows the correlation and p-value of each metric across each project.

\begin{center}
\captionof{table}{List of P-values, Correlations, and Metrics}%
\begin{longtable}{| p{.40\textwidth} | p{.40\textwidth} | p{.20\textwidth} |}
	\hline
	\textit{Metric} & \textit{Correlation} & \textit{two-tailed p-value} \\
    \hline
	IC-NOC & -0.0897 & 0.060 \\
	IC-DIT & 0.0585 & 0.155 \\ 
	IC-LCOM1 & 0.195 & 0.014 \\
	IC-WMC & 0.0189 & 0.643 \\ 
	IC-RFC & 0.598 & 2.15e-17 \\
  LOC & 0.439 & 2.027e-31 \\
	\hline
\end{longtable}
\addtocounter{table}{-1}%
\label{combined_pearsons}
\end{center}


Overall, we see very good p-values for IC-NOC, IC-RFC and IC-LCOM1.
A positive correlation in IC-RFC and IC-LCOM1 suggests that as these values increase, the number of bugs fixed also increase.
Likewise, the negative correlation for IC-NOC suggests that as more projects begin to rely on this one, the number of bugs fixed in a release goes down.

These results are the opposite of what we expected.
We had thought that as the number of children went up, it would be the result of a good project (hence, fewer bugs).
Additionally, as the number of publicly exposed functions go up, we would expect the project quality to go down (more bugs) due to the nature of encapsulation.

The results of IC-LCOM1 could have gone either way; including libraries to utilize one feature, if that feature is well-designed, may reduce the complexity of the project we are examining.
These results suggest that doing this (a high LCOM means there are many unused dependencies) does increase the number of bugs fixed in a release.

Despite some strong correlations, we see a huge variation of correlation between a metric and the bug count between projects.
In figures \ref{dit_bugs_vs_pval}-\ref{wmc_X_vs_pval}, which show the p-value mapped against either the bug count or the number of release, we do not see anything that looks like a trend.
However, some really large values have a really strong correlation, and some really small values have a really weak correlation.
Based on this evidence, we started hypothesizing what it might be which would demarcate the projects that had a strong correlation, and the projects which had a weak correlation.

The projects with strong correlation of IC-DIT:
\begin{itemize}
\item LOG4J2
\item CAMEL
\item AMBARI
\end{itemize}

We found that if one took the number of releases divided by the number of bugs, we ended up with a graph where all the projects that had a strong correlation were grouped into the bottom left corner; this means that we can identify those projects for which these metrics have meaning, and possibly ignore the others.

As to why this metric is valuable for some projects, but not others, can be explained by looking at the operation we did to single out these projects.
Number of releases divided by number of bugs, notable the inverse of the number of bugs per release, gives us a good indication of how long the project has been around, and how active the development has been.
We believe that only projects of sufficient scale will exhibit behavior consistent with our analyses; therefore, only those projects will have this correlation.
In the graphs from figures \ref{dit_bugs_vs_pval}-\ref{wmc_X_vs_pval} this value is what "Activity" is referring too.
To verify this, we will need to run more analysis on larger projects.

Below, we present a number of graphs.
Figures \ref{accumulo} trough \ref{log4j2} show the complexity of projects over a period of time.
The blue line represents the number of bugs fixed in each version.
Our goal is to find correlations between this blue line and all the other lines; the good news is that we see a correlation in many of these projects.
The bad news is that some projects don't have this correlation, which leads us to the remainder of the graphs.

Figures \ref{dit_bugs_vs_pval}, \ref{lcom1_bugs_vs_pval}, \ref{rfc_bugs_vs_pval}, \ref{wmc_bugs_vs_pval} show the number of totals bugs in a project versus the confidence level of our metrics being correlated to the number of bugs.
Each graph shows the confidence of a single metric, as described in their titles.
There does not seem to be a correlation, beyond a few projects that are where we expect (Camel, Ambari).
Note the great vertical spread.

Figures \ref{dit_releases_vs_pval}, \ref{lcom1_releases_vs_pval}, \ref{rfc_releases_vs_pval}, \ref{wmc_releases_vs_pval} show the number of releases a project has versus their confidence level for a given metric.
We seem a much greater spread head, not quite as horizontal.

For the remainder of the figures, \ref{dit_X_vs_pval}, \ref{lcom1_X_vs_pval}, \ref{rfc_X_vs_pval}, \ref{wmc_X_vs_pval}, we took the number of releases and divided it by the number of bugs.
The previous two types of graphs is what led us to do this; neither of them was perfect, but each had some value.
Using this method, we can see a much stronger correlation between the confidence level and the value.
In some of these graphs, LOG4J2 is mis-classified due to the way bugs are counted by their project managers and by the way it was put into SMF.
They would put all bugs towards a release identifier, but not tag that release in the source code; this meant that none of those releases with many bugs had metric calculations.


\section{Conclusion}

%% Objective: we defined interactive complexity and it's sub measures, showed that there is a positive correlation between IC and fault rate. Now, pull it back to talking about how complexity from other domains can be adapted to this one and successfully used

In this chapter, we defined and implemented Interactive Complexity; a software complexity measure inspired by complexity measures in ecology.
Each of the 5 metrics which compose interactive complexity were compared to the fault rate of released versions of software projects, using bug tracking tools to identify the fixed bugs per version.
Correlations between those metrics and the fault rates were identified, with some metrics having strong correlations and other being less successful.

In order for many of these metrics to have meaning to a particular project, there is a set of criteria which must be met.
Consider an application which has a single commit, and consists of a very small number of lines.
The scale of this project, both in terms of codebase and time in which it has existed, is very limited.
Without scale, it becomes difficult to say anything meaningful about the application.
This is similar to saying that we need a sufficient sampling to find a pattern in a radio signal.
For interactive complexity to have meaning, projects need to have a sufficient number of releases, recorded bugs and fixes, and scale (measured in lines of code).
Table \ref{ic:projects_and_values} shows the raw results; in this table, projects with a low number of releases and bugs often have no, or meaningless, values for the complexity metrics.

However, if we don't segment the data, and instead look at it strictly as a collection of data points with no grouping, we can analyze even those small projects.
This is exactly what table \ref{combined_pearsons} does.
We can see a very strong correlation to lines of code and bugs fixed per release; this is confirming the hypothesis that projects of sufficient scale will demonstrate some identifiable pattern of complexity.

The number of children (IC-NOC) metric did not have a strong correlation with bugs fixed per release, but was on the cusp of having statistical significance.
The negative correlation suggests that a large value here means fewer bugs per release.

Depth of Inheritance Tree (IC-DIT) had an even weaker correlation.
This makes sense, given that there will only be a small variation in this value between most projects.
Some "core" projects will have a lower value (such as LOG4J2), but anything that includes a fair number of projects will have a similar value; 6-degrees suggests that the one project which raises the value to 6 or 7 will be hit.

Lack of Cohesion of Methods (IC-LCOM1) gives a much strong correlation; p = 0.014.
In many ways, high values here can indicate a poorly maintained project, which indicates a lack of attention to detail on the part of the project maintainers.
As such, it is not surprising that we have a strong correlation.
The positive correlation suggests more bugs per release if this value is high.

Weighted Methods per Class (IC-WMC) has a very weak correlation, suggesting that the number of libraries included has no bearing on the quality of the project in question.
The original hypothesis was that this value would be strongly correlated, and provide a way of quantifying dependency hell.

Response Set for Class (IC-RFC) has an exceptionally strong correlation, however, there is caveat.
This measure is a count of the number of publicly exposed methods and classes.
In many ways, this could be considered a stand-in for the scope of the project, and since we know that larger projects tend to have bugs, and have larger development teams to fix those bugs, it is sensible that this value is so strongly correlated.
Similarly, lines of code (LOC), our baseline, has a strong connection.

In essence, the application of these 5 measures results in strong correlations for 3 of them, and weak or no correlations for 2.
The attempt to apply complexity theory from an ecosystem perspective is a partial success; some of the analogies translated well, others did not.
The analogies that translated well included:
\begin{itemize}
	\item The number of channels available to interact with the environment will be related to the complexity of the organism (IC-RFC)
	\item A greater number of organisms that rely on this organism, the more likely it is to be a keystone species (IC-NOC)
	\item An organism which relies on a number of other organisms for only one aspect of it's survival is more vulnerable to trouble (OC-LCOM1)
\end{itemize}

The analogies that did not translate are:
\begin{itemize}
	\item The depth of a food web is related to the complexity of the organisms in the food web (IC-DIT)
	\item An organism that relies on a large ecosystem is more vulnerable to trouble (IC-WMC)
\end{itemize}

However it is not clear that the last two analogies would hold up consistently in the biological world either. Fresh water food webs tend to be smaller, and involve less complex organisms than oceanic food webs, but that rule may not generalize in comparing, say, food chains in fresh water mud and oceanic mud. This demonstrates that there may be some overlap between software complexity and other forms of complexity, but reaffirms that domain-specific measures may still be needed. Complexity is complicated.

\pagebreak

\begin{center}
	\centering
	\captionof{table}{List of Projects and Values}%
  \begin{longtable}{|p{.20\textwidth} | p{.10\textwidth} | p{.10\textwidth} | p{.10\textwidth} | p{.10\textwidth} | p{.12\textwidth} | p{.08\textwidth} |}
	\hline
    \textit{Project} & \textit{IC-DIT} & \textit{IC-WMC} & \textit{IC-RFC} & \textit{IC-LCOM1} & \textit{LOC} & \textit{Bugs} \\
	\hline
    \endhead
	\input{./interactive_complexity/tableValues}
	\hline
	\end{longtable}
\addtocounter{table}{-1}%
\label{project_list}

\noindent (80 rows) \\
\end{center}

\pagebreak

\begin{center}
  \captionof{table}{List of p-values, correlations, and metrics for each individual project}%
  \label{ic:projects_and_values}
	\begin{longtable}{| p{.30\textwidth} | p{.30\textwidth} | p{.20\textwidth} | p{.20\textwidth} |}
	\hline
		\textit{Project Name} & \textit{Metric} & \textit{Correlation} & \textit{two-tailed p-value} \\
	\hline
    \endhead
\input{./interactive_complexity/tableValuesPValues.txt}
	\hline
\end{longtable}

  \noindent (80 rows) \\
  \addtocounter{table}{-1}%
  \label{p_values}
\end{center}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.75]{./interactive_complexity/images/ACCUMULO-metrics_vs_bug_total.png}
	\caption{Apache Accumulo}
	\label{accumulo}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.75]{./interactive_complexity/images/HTTPCLIENT-metrics_vs_bug_total.png}
	\caption{Apache HTTPComponents Client}
	\label{httpclient}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.75]{./interactive_complexity/images/RAVE-metrics_vs_bug_total.png}
	\caption{Apache Rave}
	\label{rave}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.75]{./interactive_complexity/images/CXF-metrics_vs_bug_total.png}
	\caption{Apache CXF}
	\label{cxf}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.75]{./interactive_complexity/images/LOG4J2-metrics_vs_bug_total.png}
	\caption{LOG4J2}
	\label{log4j2}
\end{figure}

\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-DIT-number_of_bugs_vs_pval.png}      
        \caption{IC-DIT Number of Bugs vs pval}                      
        \label{dit_bugs_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-DIT-number_of_releases_vs_pval.png}  
        \caption{IC-DIT Number of Releases vs pval}                          
        \label{dit_releases_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-DIT-random_vs_pval.png}
        \caption{IC-DIT Activity vs pval}
        \label{dit_X_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-LCOM1-number_of_bugs_vs_pval.png}
        \caption{IC-LCOM1 Number of Bugs vs pval}
        \label{lcom1_bugs_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-LCOM1-number_of_releases_vs_pval.png}
        \caption{IC-LCOM1 Number of Releases vs pval}
        \label{lcom1_releases_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-LCOM1-random_vs_pval.png}
        \caption{IC-LCOM1 Activity vs pval}
        \label{lcom1_X_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-RFC-number_of_bugs_vs_pval.png}
        \caption{IC-RFC Number of Bugs vs pval}
        \label{rfc_bugs_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-RFC-number_of_releases_vs_pval.png}    
        \caption{IC-RFC Number of Releases vs pval}                        
        \label{rfc_releases_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-RFC-random_vs_pval.png}
        \caption{IC-RFC Activity vs pval}
        \label{rfc_X_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-WMC-number_of_bugs_vs_pval.png}
        \caption{IC-WMC Number of Bugs vs pval}
        \label{wmc_bugs_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-WMC-number_of_releases_vs_pval.png}
        \caption{IC-WMC Releases of Bugs vs pval}
        \label{wmc_releases_vs_pval}
\end{figure}
\begin{figure}[!htb]
        \centering     
        \includegraphics[scale=0.75]{./interactive_complexity/images/IC-WMC-random_vs_pval.png}
        \caption{IC-WMC Activity vs pval}
        \label{wmc_X_vs_pval}
\end{figure}
