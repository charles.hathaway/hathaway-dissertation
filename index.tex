\documentclass{thesis}

\usepackage[style=trad-abbrv]{biblatex}
%\usepackage{wordlike}
%\usepackage{setspace}
%\usepackage{parskip}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{adjustbox}
\usepackage{float}
\usepackage{longtable}
\usepackage{lmodern}  % for bold teletype font
\usepackage{amsmath}  % for \hookrightarrow
\usepackage{xcolor}   % for \textcolor
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{wrapfig}
%\usepackage[toc,page]{appendix}


\lstset{
  basicstyle=\ttfamily,
  columns=fullflexible,
  frame=single,
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space},
}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\bibliography{biblo}

%%%%%%%%%%%%%%%%%%%%  supply titlepage info  %%%%%%%%%%%%%%%%%%%%%
\thesistitle{\bf BROADENING THE APPLICABILITY OF SOFTWARE COMPLEXITY METRICS WITH BIOLOGICAL ANALOGUES}
\author{Charles Hathaway}
\degree{Doctor of Philosophy}
\department{Computer Science}
\signaturelines{5} 
\thadviser{Dr. Ron Eglash} %if needed
\cothadviser{Dr. Mukkai Krishnamoorthy}
\memberone{Dr. David Spooner} %if needed
\membertwo{Dr. Ana Milanova} %if needed
\memberthree{Dr. Bruce Piper} %if needed
\submitdate{[May 2018]\\Submitted March 2018}
%%%%%%%%%%%%%%%%%%%%%   end titlepage info  %%%%%%%%%%%%%%%%%%%%%%
%just checking
\begin{document}

\titlepage
\copyrightpage
\tableofcontents       % required
\listoftables          % required if there are tables
\listoffigures         % required if there are figures

% Hypothesis: Software complexity metrics have similar attributes to conventional complexity as explored by Shannon and others

\specialhead{Abstract}
\begin{abstract}
Software metrics represent an application of conventional computer science to itself in an attempt to quantify the complexity of software systems.
Many of the conventional metrics proposed in academia and industry find their roots in a top-down composition that has parallels to the way conventional AI has been done prior to the surge in machine learning techniques.
As software becomes increasingly networked together, the need for bottom-up approaches to software complexity becomes more apparent.
The complexity concepts created for biological systems, such as ecological biodiversity, neural pattern recognition, and genetic mutation rate, offer "proof of concept" illustrations that show how such bottom-up metrics can provide insights into the behavior of complex systems.
Thus computational analogs to these biological complexity metrics may offer deeper understanding of how complexity occurs in contemporary networked computing systems.
This dissertation compares traditional software metrics to these bio-inspired measures in several contexts, applying them to tasks such as bug-finding, measuring student comprehension, and using metrics as a way of quantifying generative justice (a form of justice which originates from the groups that require it, rather than an external entity).

Chapters \ref{interactive_complexity} and \ref{taint_analysis} compare taint analysis, measured using standard complexity metrics, with an ecosystem approach that examines interactive complexity.
Chapter \ref{icedutech} examines the use of traditional complexity metrics to measure student comprehension of computer science basics.
A discussion of how traditional metrics fail to enable critical examination of participation diversity in open source projects, in contrast to entropy measures inspired by biodiversity, is provided in chapter \ref{generative_justice}.

Combining these bio-inspired and traditional metrics utilizing machine learning techniques, also inspired by biology and neurology, chapter \ref{machine_learning} demonstrates that greater results can be achieved with both angles than a single perspective.

\end{abstract}

\chapter{Introduction} \label{introduction}

Analogies to biological processes have given the scientific community insights which allowed the development of genetic algorithms, subsumption robotics, and nanotechnology.
This dissertation draws parallels between those same biological processes and software complexity metrics; algorithms which perform the unique function of quantifying the complexity of software systems.
Complexity is a profound parameter that can allow us to explore both human and machine dimensions of software systems.

For example, contrast the etymology of complexity and complicated.
"Complicated" is from the roots  \textit{com} (meaning "together") and  \textit{plic} (meaning "folded").
It simply means many layers. But "complexity" comes from com and \textit{plex}, meaning "woven"--in other-words, interdependencies.
Thus, complexity measures how "interwoven" a system is: the biodiversity of an ecosystem; the weaving of harmonies in an orchestra, or other systems whose whole is greater than the sum of its parts.

\begin{wrapfigure}{r}{0.25\textwidth}
		\begin{adjustbox}{max width=0.25\textwidth}
			\includegraphics{./images/800px-Margaret_Hamilton_-_restoration.jpg}
		\end{adjustbox}
		\caption{Margaret Hamilton standing next to code listing developed for the Apollo 11 mission}
		\label{apolloCode}
\end{wrapfigure}

In software metrics, complexity was formerly introduced by McCabe circa 1976 \cite{mccabe1976complexity}.
However, there are examples of people considering software complexity before his publication; consider the iconic image (pictured in figure \ref{apolloCode}) of Margaret Hamilton standing next to the code listing developed by her and the team at NASA for the Apollo mission in 1969 \cite{hamilton1969apollo}.
This image is interesting because it is intended to instill awe at the complicated software developed for this task; in this case, measured in Margarets (somewhere between 0.9 and 1.0 Margarets, I would say).

An astute observer may notice the use of complicated, rather than complexity, in describing this image.
This fine distinction is essential for many of the theories discussed further in this paper.
If we accept "Margarets" as a stand-in for lines of code, one of the most prolific complexity measures, we can understand this image as providing a visual for the size of the codebase, rather than the complexity.
In other words, if each of those pages contains the lines "All work and no play makes Jack a dull boy", merely looking at the number of Margarets will imply very complicated code, whereas the complexity measured by other kinds of metrics may be very low.
%% CHH: Need citation here
Admittedly, previous work has found very strong connections between lines of code and measures of software quality. % CHH: citations
But that is no different than saying that the odds of finding a spelling error increase with the length of an essay. What we really want to know is what makes one application more complex than another, given similar codebase sizes? How do we explain that certain features with no relation to codebase size seem to matter a great deal in the challenge they present to developers; the likelyhood of finding faults, or the amount of time required? % CHH: there is a paper which finds this; what was it?

\section{Contributions and Outline}

Chapter \ref{lit_review} surveys previous works in the field of software complexity, including well-known metrics such as Cyclomatic Complexity.
Many of these metrics utilize the top-down often observed in good old-fashioned AI; for each metric, the author presents what they believe has a particular use within the domain of complexity metrics, explain how they designed it, and often gives a use case where it was applied.
The author often believes that the metric they are presenting provides some advantage when compared to existing metrics.
Generally, this advantage is achieved by accounting for something that was left out by previous metrics.
Data-Flow complexity adds the flow of information within an application to cyclomatic complexity, thus the author claims it provides a better estimation of the difficulty of comprehension for a software project. Since complexity is such a fundamental and global property, these are only a few of the many applications that would be possible if we had a robust complexity metric.
But how to achieve these broader applications? 

The measures discussed in chapter \ref{lit_review} are often applied only to software projects; chapter \ref{icedutech} examines their use in education.
In the context of biological analogies, the question is whether these GOFAI measures can be used to quantify student comprehension; whether they can serve as a measure for the other end of the brain-computer pipeline.
% idk man, this needs some work...

Chapter \ref{generative_justice} uses Shannon entropy, often associated with the complexity of environment and organisms, to provide a framework for examining diversity within a developer community and discuss how this diversity can be a vehicle for generative justice.
Generative justics is a mechanism whereby a group retains the fruits of their own labor, and in doing so can gain economic justice in the face of adversity.

Chapters \ref{interactive_complexity} and \ref{taint_analysis} contrast each other through examination software product using different techniques.
Interactive Complexity, a metric designed to take a network biology-inspired look at software systems, is used in chapter \ref{interactive_complexity} to examine many projects and search for connection between various interactive complexity metrics and fault rate.
In contrast, chapter \ref{taint_analysis} uses traditional software metrics to predict information taints within Android application.
An information taint occurs when a private piece of private information is passed off of the device the program is executing; for example, the contact list being sent to a web server.

Machine learning is a field which draws many inspirations from biology, and chapter \ref{machine_learning} uses those inspirations alongside the interactive complexity to predict fault rates of specific source files.
For example, neural networks take their name from the observation of a brains neural pathways; using software metrics, we are able to provide the features for a neural network to operate on, and can test whether these features are an indicator of source code which will contain many faults.

\chapter{Previous Works} \label{lit_review}

\input{./LiteratureReview/body.tex}

\chapter{Use of Software Metrics in Evaluating Student Comprehension} \label{icedutech}
\blfootnote{This chapter previously appeared as: \textbf{Techniques for computer-assisted grading in computer science curricula} \cite{hathawaytechniques}}
\input{./icedutech2015/body.tex}

\chapter{Quantitative Metrics for Generative Justice} \label{generative_justice}
\blfootnote{This chapter previously appeared as: \textbf{Quantitative metrics for generative justice} \cite{callahan2016quantative}}
\input{./brian_complexity/body.tex}

\chapter{Interactive Complexity: Software Metrics from an Ecosystem Perspective} \label{interactive_complexity}
\blfootnote{This chapter previously appeared as: \textbf{Interactive Complexity: Software Metrics from an Ecosystem Perspective} \cite{hathaway2017interactive}}
\input{./interactive_complexity/body.tex}

\chapter{Taint Analysis and Correlation with Complexity} \label{taint_analysis}
\input{./taint_analysis/body.tex}

\chapter{Machine Learning and Software Complexity Metrics} \label{machine_learning}
\input{./ml_and_software_complexity/body.tex}

\chapter{Software Metrics Framework} 
\blfootnote{This chapter previously appeared as: \textbf{Software Metric Framework} \cite{hathaway2017smf}}
\label{software_metric_framework}
\input{./software_metrics_framework/body.tex}

\chapter{Conclusions and contributions} \label{conclusion}

%% OUTLINE
% Rephrase introduction
% Connect each main point back to the central theme; discuss findings
%% One point for each chapter?
% Final conclusion; was the hypothesis correct?

While acknowledging that complexity will perhaps never be reduced to a single measure for all domains and cases, we have demonstrated that several of these metrics can provide insight to the specific problem of software complexity.
Each chapter discussed the application of metrics to a specific domain; finding the deep relationships between these metrics remains an open area of research.
This chapter attempts to find a unifying conclusion that will give future authors and researchers a basis for their work.

Using analogies to complexity in nature to deepen the understanding of software complexity has already occurred in some of the literature. 
%% CHH: Find the below citation, or change the name
A common approach to examining complexity, first proposed by Kolmogorov and Chaitin, was using randomness as a metric. In the audio domain, the least random noise is a sine wave or a constant tone. The most random is white noise; the static you hear on the radio when nothing is being broadcast. But as James Crutchfield \cite{crutchfield1996turbulent} explains, that is very unsatisfying when we think about how the brain reacts to white noise: we fall asleep. And computers can simulate white noise with a simple random function.  In creating his alternative metric, computational complexity, he noted that the features we admire in nature, such as ecological biodiveristy, swirling turbulence in fluids, and so on are neither predictable like the sine wave nor simply random like white noise: they fall somewhere in the middle. For that reason they are better characterized as maximizing "computational complexity" rather than statistical randomness. Returning to the audio example, a constant tone is boring, and white noise is boring, but music is fascinating. The "liveliness" of music is like the liveliness of life itself; partly rhythmic and partly surprising. Biological analogies for software complexity tap into this sense that well crafted code has a kind of liveliness to it.

Consider for example a student who does not know how to use an iterative loop. Copying the same lines over and over creates a structure that is too periodic. At the other end of the spectrum, a frequent error for new software students is that they create "spaghetti code" that is too random. Thus we need a software metric that can be maximized when robust structures are crafted between these extremes.  Thus our exploration of the case of student comprehension in chapter \ref{icedutech}; the hypothesis was that students who can think computationally will create programs with more complexity than those who are struggling to grasp the concepts.
Experimenting with metrics for this complexity is not just an attempt to find practical use for these measures; it is a deeper look into what constitutes robust, lively code. To that end, 3 complexity and one "complication" metric were examined: cyclomatic, data-flow, normalized-compression distance, and lines of code.

Normalized-compression distance was the closest to a generic complexity algorithm that was investigated. It is based on Komolgorov complexity, so it would maximize for the most random samples. In the small experiment carried out during that chapter, this metric performed very poorly. The sample size was too small to draw any conclusions (the experiment was just to show that such data could be obtained and measured), but if the same relationship is confirmed in larger samples, it could help to establish the idea that software is, like the structures in nature that Crutchfield contemplates, best understood with measures that capture its "liveliness" than with simple metrics for unpredictability.

Lines of code has an interesting purpose in this case; although it usually can be used a good indicator of fault rate (see the discussion in chapter \ref{introduction}), that does not apply to student comprehension. As a simple example, consider the case of asking a student to create a program to generate a sequence of colored dots in some uniform pattern. If they simply hard-code the pattern, they can get the answer correct without demonstrating any understanding of branch statements or control flow. The total number of lines of code can increase indefinetly without ever making a dent in their control flow comprehension. Similarly, a polluted lake may have an enormous mass of pond scum, but all animal life has been killed off. You can increase lake area or biomass indefinetly without ever making a dent in the biodiversity. Lines of codes and square miles of nature are a necessary but insufficient condition to sustain either type of liveliness. As such they are perfect measures for normalizing a complexity metric; comprising the denominator in a ratio that reflects how much liveliness per unit of existance. 


Chapter \ref{generative_justice} approaches a similar question, moving from the liveliness of code to the liveliness of the coding community. It should now be easier  to now see why there might be problems with shallow examinations of coding community diversity, and why a deeper look at biological analogies might be helpful. The shallow approach may dis-proportionally represent certain roles within the software development life cycle, just as surveys of biodiveristy such as "number of species" could overlook the fact that 99\% of biomass is pond scum. Normalizing ecosystems to see how much each species participates in the nitrogen cycle or carbon cycle is parallel to normalizing coding communities to look at its participatory diversity.

In that chapter, we examine the use of Shannon entropy and normalized contributory diversity to analyze a representational sampling of a micro-community.
Using blog posts as stand-ins for the development lifecycle, we quantify the participation diversity of the group.
Shannon entropy is then used to provide a baseline for what is expected, and to produce a generic number which should be applicable to many cases.
This is an example of augmenting a domain-specific metric with a generalized complexity metric to arrive at a more general answer.

To strengthen the argument that traditional software complexity metrics do not currently provide perfect means of measuring importain phenomena that arise in software, chapter \ref{taint_analysis} attempts to use them as an indicator of information leakage in Android application.
Information leakage is the act of a software application (in this case, Android Application, or "app") sending sensitive information off the system, whether to a file, network server, or other location which can no longer be controlled by the application.
Traditionally, this task has been examined by using static semantic analysis to identify segments of code which were transferring private information to these locations.
Attempting to use complexity more generalized than this task was not successful; no positive correlation was found in this chapter.

However, this chapter prompted the creation of the Software Metric Framework.
Discussed in chapter \ref{software_metric_framework}, the case studies described in chapters \ref{interactive_complexity} and \ref{machine_learning} to take place in an effective manner.

Chapter \ref{interactive_complexity} proposed a new set of complexity metrics, inspired by analogs in the domains of biology and ecology, to measure the complexity of software systems.
These metrics were found to be effective at predicting fault rate within software projects, after conducting a study on 87 large projects, each with dozens of releases and data points.

Finally, chapter \ref{machine_learning} took this bio-inspired metrics, the traditional metrics examined in other chapters, and combined them with machine-learning models which are not specific to software complexity; the results showed great potential. 



\section{Future Works}

The work done in chapter \ref{icedutech} would be greately benefit from a larger experiment.
To this end, over 5000 student submissions have been archived on the CSDT Community site.
If a survey were added, very similar to the one done in that chapter, or teacher's grading scores added, this experiment could be done on a much larger scale to conclude some statistical significant insights.

Along with expanding on the work in chapter \ref{icedutech}, this data can also be applied to measuring of project mutation over the course of it's life.
This has two meanings; the first, the mutation that naturally occurs as a student progresses through an assignment, and the second being the mutation as the project gets copied and modified by a students peers.
This second meaning is of particular interest, especially if the projects mutate in a way that is consistent with mutations we see in nature.
To the that end, each of this projects has been tagged with a parent project, if applicable.
Using this information, it should be possible to visualize the mutation over a period of alterations.

Chapter \ref{generative_justice} conducted an experiment using stand-in data; with sufficient time, it may be possible to conduct such a survey with data more aligned to the domain.
If a measure could be found that would provide a fair representation of contribution, it would enable the development of a participatory diversity metric. 
This would be helpful in determining who key developers or movement makes in a project are, or gaging a project's diversity participation.

Interactive complexity, as described in chapter \ref{interactive_complexity}, showed great promise on very large projects.
If a tool was developed to allow easier use of this metric suite, it may prove helpful in predicting project success in the open source market place.
This would be important for developers selecting projects to include in their own endeavors, especially when including such projects bring with it the risk of a security vulnerability.

Particular of interest for its potential for industry application is chapter \ref{machine_learning}.
Although this chapter was written using a granularity of file-level metrics, it is feasible that such an analysis could be done at the method or line level.
This could give developers another approach to scanning software for vulnerabilities, particularly useful in languages with have scarce resources (such as domain specific languages, like MUMPS).

\section{Other contributions}

In addition to the contributions discussed in the conclusions section, this dissertation has resulted in a number of secondary artifacts.
Of particular note is the Software Metrics Framework described in chapter \ref{software_metric_framework}; with this tool, reproducing any of these experiments should be relatively easy. It is my hope that such combinations of rigorous quantative measures and inspirations from biological and social worlds can make these metrics a path towards broadening the applications of software complexity and deepening its abilities for research.

Several of the chapters contained in this dissertation were published as papers, including:

\vspace{5mm}

List of published papers \\
\begin{itemize}
	\item Culturally Situated Design Tools: Generative Justice as a Foundation for STEM Diversity \cite{eglash2017culturally}
	\item Quantative Metrics for Generative Justice \cite{callahan2016quantative}
	\item International Conference on Educational Technologies \cite{hathawaytechniques}
	\item How Open is Open Source?\\Mutation Rate as a Metric for Generative Justice \cite{hathaway2014generative}
	\item Interactive Complexity: Software Metrics from an Ecosystem Perspective
	\item Software Metrics Framework
\end{itemize}

\begin{singlespace}
	\printbibliography
\end{singlespace}

\end{document}
